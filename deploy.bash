#!/bin/bash

usage() {
cat <<EOF
usage: $0 -u wchen -l /home/wen/git/prax/Prax/war [-h moondog.io][-r /var/www/prax]

This script deploys prax war directory from localpath to a remote path.

OPTIONS:
   -u      Username to ssh into remote host
   -l      Local path
   -h      Remote host
   -r      Remote path
   -?      Shows this message
EOF
exit
}

LOCAL_PATH=
REMOTE_PATH="/var/www/prax"
REMOTE_USER=
REMOTE_HOST="moondog.io"

while getopts ":u:h:l:r:" OPTION
do
     case $OPTION in
         u) REMOTE_USER=$OPTARG;;
         h) REMOTE_HOST=$OPTARG;;
         r) REMOTE_PATH=$OPTARG;;
         l) LOCAL_PATH=$OPTARG;;
         ?) usage;;
     esac
done

if [ $# -eq 0 ]; then
    usage
elif [ -z $LOCAL_PATH ]; then
    echo "LOCAL_PATH was not set. See usage for more info."
    exit 1
elif [ ! -d $LOCAL_PATH ]; then
    echo "$LOCAL_PATH does not exist. See usage for more info."
    exit 1
elif [ -z $REMOTE_HOST ]; then
    echo "REMOTE_HOST was not set. See usage for more info."
    exit 1
elif [ -z $REMOTE_USER ]; then
    echo "REMOTE_USER was not set. See usage for more info."
    exit 1
elif [ -z $REMOTE_PATH ]; then
    echo "REMOTE_PATH was not set. See usage for more info."
    exit 1
else
    echo "Starting to deploy $LOCAL_PATH to $REMOTE_USER@$REMOTE_HOST:$REMOTE_PATH.";
    
    mv $LOCAL_PATH/WEB-INF $LOCAL_PATH/../tmp

    scp -r -p $LOCAL_PATH/* $REMOTE_USER@$REMOTE_HOST:$REMOTE_PATH
    if [ $? -eq 0 ]; then
	echo "$LOCAL_PATH was successfully deployed."
    else
	echo "An unexpected error occurred while deploying $LOCAL_PATH."
    fi

    mv $LOCAL_PATH/../tmp $LOCAL_PATH/WEB-INF
fi