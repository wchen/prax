package com.praxiteles.test;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.google.gwt.junit.tools.GWTTestSuite;
import com.praxiteles.library.model.DashboardIntegration;
import com.praxiteles.library.model.IconInfoIntegration;
import com.praxiteles.library.model.IconIntegration;
import com.praxiteles.library.model.MenuIntegration;
import com.praxiteles.library.model.MenuOptionIntegration;
import com.praxiteles.library.model.ModelVirtualProxyIntegration;
import com.praxiteles.library.model.WorkspaceIntegration;

public class ModelSuite extends GWTTestSuite {
	  public static Test suite() {
		    TestSuite suite = new TestSuite("Suite test for all models.");
		    suite.addTestSuite(DashboardIntegration.class); 
		    suite.addTestSuite(IconInfoIntegration.class);
		    suite.addTestSuite(IconIntegration.class);
		    suite.addTestSuite(MenuIntegration.class);
		    suite.addTestSuite(MenuOptionIntegration.class);
		    suite.addTestSuite(ModelVirtualProxyIntegration.class);
		    suite.addTestSuite(WorkspaceIntegration.class);
		    return suite;
		  }
}
