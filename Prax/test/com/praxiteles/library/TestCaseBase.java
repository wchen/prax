package com.praxiteles.library;

import junit.framework.TestCase;

import org.junit.Before;

public abstract class TestCaseBase extends TestCase {

	public TestCaseBase(String name) {
		super(name);
	}

	@Override
	@Before
	protected void setUp() throws Exception {
		preSetUp();
		super.setUp();
		loadData();
		postSetUp();
	}

	protected void loadData() {

	}

	protected void preSetUp() {

	}

	protected void postSetUp() {

	}

}
