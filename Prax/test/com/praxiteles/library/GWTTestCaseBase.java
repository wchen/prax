package com.praxiteles.library;

import com.google.gwt.junit.client.GWTTestCase;

public abstract class GWTTestCaseBase extends GWTTestCase {

	@Override
	protected void gwtSetUp() throws Exception {
		preSetUp();
		super.gwtSetUp();
		loadData();
		postSetUp();
	}
	
	protected void loadData() {

	}

	protected void preSetUp() {

	}

	protected void postSetUp() {

	}
}
