package com.praxiteles.library.model;

import java.util.ArrayList;

import org.junit.Test;

import com.praxiteles.library.ModelTestBase;

public class MenuOptionIntegration extends ModelTestBase {

	@Override
	protected void preSetUp() {
		instance = new MenuOption();
	}
	
	@Override
	protected void loadData() {
		jsonData = "{"
				+"    \"name\": \"New\","
				+"    \"event\": \"showMenuOptions\","
				+"    \"img\": \"new\","
				+"    \"menu_options\": ["
				+"        {"
				+"            \"name\": \"Application\","
				+"            \"event\": \"addApp\","
				+"            \"img\": \"newApp\""
				+"        },"
				+"        {"
				+"            \"name\": \"Folder\","
				+"            \"event\": \"addFolder\","
				+"            \"img\": \"addFolder\""
				+"        }"
				+"    ]"
				+"}";
		instance.setData(JSOModel.fromJson(jsonData));
	}

	@Test
	public void testGetName() {
		assertEquals("New", getField(MenuOption.name));
	}
	
	@Test
	public void testGetEvent() {
		assertEquals("showMenuOptions", getField(MenuOption.event));
	}
	
	@Test
	public void testGetImg() {
		assertEquals("new", getField(MenuOption.img));
	}
	
	@Test
	public void testMenuOptions() {
		ArrayList<MenuOption> menuOptions = getArrayField(MenuOption.menuOptions);
		assertEquals(ArrayList.class, menuOptions.getClass());
		assertEquals(2, menuOptions.size());
		
		MenuOption theMenuOption = menuOptions.get(0);
		assertEquals(MenuOption.class, theMenuOption.getClass());
	}
}
