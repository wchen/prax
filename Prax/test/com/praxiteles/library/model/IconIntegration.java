package com.praxiteles.library.model;

import org.junit.Test;

import com.praxiteles.library.ModelTestBase;

public class IconIntegration extends ModelTestBase {

	@Override
	protected void preSetUp() {
		instance = new Icon();
	}
	
	@Override
	protected void loadData() {
		jsonData = "{"
				+"    \"id\": 1,"
				+"    \"name\": \"regApp\","
				+"    \"label\": \"Regular Application\","
				+"    \"img\": \"1\","
				+"    \"x\": 1,"
				+"    \"y\": 1,"
				+"    \"type\": 1,"
				+"    \"auxiliary\": {"
				+"        \"url\": \"http://www.google.com\""
				+"    }"
				+"}";
		instance.setData(JSOModel.fromJson(jsonData));
	}

	@Test
	public void testGetId() {
		assertEquals(1, getField(Icon.id));
	}

	@Test
	public void testGetName() {
		assertEquals("regApp", getField(Icon.name));
	}
	
	@Test
	public void testGetLabel() {
		assertEquals("Regular Application", getField(Icon.label));
	}
	
	@Test
	public void testGetImg() {
		assertEquals("1", getField(Icon.img));
	}

	@Test
	public void testGetX() {
		assertEquals(1, getField(Icon.x));
	}
	
	@Test
	public void testGetY() {
		assertEquals(1, getField(Icon.y));
	}
	
	@Test
	public void testGetType() {
		assertEquals(1, getField(Icon.type));
	}
	
	@Test
	public void testIconWithInfo() {
		assertEquals(IconInfo.class, getField(Icon.auxiliary).getClass());
	}
}
