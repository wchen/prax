package com.praxiteles.library.model;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

import com.praxiteles.library.ModelTestBase;
import com.praxiteles.library.field.ArrayListField;
import com.praxiteles.library.field.BooleanField;
import com.praxiteles.library.field.DoubleField;
import com.praxiteles.library.field.Field;
import com.praxiteles.library.field.IntegerField;
import com.praxiteles.library.field.ModelField;
import com.praxiteles.library.field.StringField;

/**
 * Array of mixed values is not supported
 * Array of arrays is not supported
 * Array of values such as integer, double, and boolean has to be in string format
 * 
 * @author Wensheng Chen
 *
 */
public class ModelVirtualProxyIntegration extends ModelTestBase {

	private HashMap<String, Field> fields;

	class SomeModel extends ModelVirtualProxy {

		@Override
		protected ModelVirtualProxy getModel(
				Class<? extends ModelVirtualProxy> modelType, JSOModel jsoModel) {
			return modelType == EmptyObject.class ? new EmptyObject(jsoModel)
					: null;
		}

	}

	class EmptyObject extends ModelVirtualProxy {

		EmptyObject() {
			super();
		}

		EmptyObject(JSOModel jsoModel) {
			super();
			super.setData(jsoModel);
		}

		@Override
		protected ModelVirtualProxy getModel(
				Class<? extends ModelVirtualProxy> modelType, JSOModel jsoModel) {
			return modelType == EmptyObject.class ? new EmptyObject(jsoModel)
					: null;
		}

	}

	@Override
	protected void preSetUp() {
		instance = new ModelVirtualProxy();
		fields = new HashMap<String, Field>();
		fields.put("int", new IntegerField("int"));
		fields.put("string", new StringField("string"));
		fields.put("double", new DoubleField("double"));
		fields.put("boolean", new BooleanField("boolean"));
		fields.put("null", new StringField("null"));
		fields.put("emptyArray", new ArrayListField<String>("emptyArray", new String(
				"")));
		fields.put("arrayOfInt", new ArrayListField<Integer>("arrayOfInt", new Integer(
				1)));
		fields.put("arrayOfBoolean", new ArrayListField<Boolean>("arrayOfBoolean", new Boolean(true)));
		fields.put("emptyObject", new ModelField<EmptyObject>("emptyObject",
				new EmptyObject()));
		fields.put("object", new ModelField<EmptyObject>("object", new EmptyObject()));
		fields.put("arrayOfObject", new ArrayListField<EmptyObject>("arrayOfObject",
				new EmptyObject()));
	}

	@Override
	protected void loadData() {
		jsonData =  "{"
					+"    \"int\": 1,"
					+"    \"string\": \"aa\","
					+"    \"double\": 1.11,"
					+"    \"boolean\": true,"
					+"    \"null\": null,"
					+"    \"emptyArray\": [],"
					+"    \"emptyObject\": {},"
					+"    \"arrayOfInt\": ["
					+"        \"1\","
					+"        \"2\""
					+"    ],"
					+"    \"array\": ["
					+"        1,"
					+"        \"aa\","
					+"        1.1,"
					+"        true,"
					+"        null,"
					+"        {"
					+"            \"int\": 3"
					+"        },"
					+"        [],"
					+"        ["
					+"            \"insideArray\""
					+"        ]"
					+"    ],"
					+"    \"arrayOfBoolean\": ["
					+"        \"true\","
					+"        \"false\""
					+"    ],"
					+"    \"object\": {"
					+"        \"int\": 1,"
					+"        \"string\": \"aa\","
					+"        \"double\": 1.11,"
					+"        \"boolean\": true,"
					+"        \"null\": null,"
					+"        \"emptyArray\": [],"
					+"        \"emptyObject\": {},"
					+"        \"arrayOfInt\": ["
					+"            \"1\","
					+"            \"2\""
					+"        ],"
					+"        \"object\": {"
					+"            \"int\": 4"
					+"        }"
					+"    },"
					+"    \"arrayOfObject\": ["
					+"    	 {\"int\": 1}, "
					+"    	 {\"int\": 2}, "
					+"    	 {\"int\": 3}"
					+"    ],"
					+"}";
		instance.setData(JSOModel.fromJson(jsonData));
	}

	@Test
	public void testGetInt() {
		assertEquals(1, getField("int"));
	}

	@Test
	public void testGetString() {
		assertEquals("aa", getField("string"));
	}

	@Test
	public void testGetDouble() {
		assertEquals(1.11, getField("double"));
	}

	@Test
	public void testGetBoolean() {
		assertEquals(true, getField("boolean"));
	}

	@Test
	public void testGetNull() {
		assertEquals(null, getField("null"));
	}

	@Test
	public void testGetNonExistent() {
		assertEquals(null, instance.get(new StringField("nonExist")));
	}

	@Test
	public void testGetEmptyArray() {
		ArrayList<String> arrayList = getArrayField("emptyArray");
		assertEquals(0, arrayList.size());
	}

	@Test
	public void testGetArrayOfInt() {
		ArrayList<Integer> arrayList = getArrayField("arrayOfInt");
		assertEquals(2, arrayList.size());
		assertEquals(new Integer(1), arrayList.get(0));
		assertEquals(new Integer(2), arrayList.get(1));
	}

	@Test
	public void testGetArrayOfBoolean() {
		ArrayList<Boolean> arrayList = getArrayField("arrayOfBoolean");
		assertEquals(2, arrayList.size());
		assertEquals(new Boolean(true), arrayList.get(0));
		assertEquals(new Boolean(false), arrayList.get(1));
	}

	@Test
	public void testGetEmptyObject() {
		SomeModel someModel = new SomeModel();
		someModel.setData(JSOModel.fromJson(jsonData));

		assertEquals(EmptyObject.class, getField(someModel, "emptyObject").getClass());
	}

	@Test
	public void testGetArrayOfObjects() {
		SomeModel someModel = new SomeModel();
		someModel.setData(JSOModel.fromJson(jsonData));

		ArrayList<ModelVirtualProxy> models = getArrayField(someModel,
				"arrayOfObject");
		assertEquals(3, models.size());

		for (int i = 0; i < models.size(); ++i) {
			assertEquals(EmptyObject.class, models.get(i).getClass());
			assertEquals(i + 1, getField(models.get(i), "int"));
		}
	}

	@Test
	public void testGetObjectOfMixedValues() {
		SomeModel someModel = new SomeModel();
		someModel.setData(JSOModel.fromJson(jsonData));
		EmptyObject emptyObj = getField(someModel, "object");

		assertEquals(1, getField(emptyObj, "int"));
		assertEquals("aa", getField(emptyObj, "string"));
		assertEquals(1.11, getField(emptyObj, "double"));
		assertEquals(true, getField(emptyObj, "boolean"));
		assertEquals(null, getField(emptyObj, "null"));
		assertEquals(null, emptyObj.get(new StringField("nonExist")));
		assertEquals(EmptyObject.class, getField(emptyObj, "emptyObject").getClass());

		ArrayList<String> arrayList = getArrayField(emptyObj, "emptyArray");
		assertEquals(0, arrayList.size());
		ArrayList<Integer> arrayList2 = getArrayField(emptyObj, "arrayOfInt");
		assertEquals(2, arrayList2.size());
		assertEquals(new Integer(1), arrayList2.get(0));
		assertEquals(new Integer(2), arrayList2.get(1));
	}

	private <T> T getField(String key) {
		return instance.get(fields.get(key));
	}

	private <T> T getField(ModelVirtualProxy model, String key) {
		return model.get(fields.get(key));
	}

	private <T> ArrayList<T> getArrayField(String key) {
		return instance.getArray(fields.get(key));
	}

	private <T> ArrayList<T> getArrayField(ModelVirtualProxy model, String key) {
		return model.getArray(fields.get(key));
	}

}
