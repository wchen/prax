package com.praxiteles.library.model;

import java.util.ArrayList;

import org.junit.Test;

import com.praxiteles.library.ModelTestBase;

public class IconInfoIntegration extends ModelTestBase {

	@Override
	protected void preSetUp() {
		instance = new IconInfo();
	}
	
	@Override
	protected void loadData() {
		jsonData = "{"
				+"    \"rclick_menu\": {"
				+"        \"menu_options\": ["
				+"            {"
				+"                \"name\": \"Launch Application\","
				+"                \"event\": \"launch\","
				+"                \"parameters\": ["
				+"                    \"arg1\","
				+"                    \"arg2\""
				+"                ]"
				+"            },"
				+"            {"
				+"                \"name\": \"Shake the earth\","
				+"                \"event\": \"shake\""
				+"            },"
				+"            {"
				+"                \"name\": \"Uninstall Application\","
				+"                \"event\": \"uninstall\","
				+"                \"parameters\": ["
				+"                    \"arg1\","
				+"                    \"arg2\""
				+"                ]"
				+"            }"
				+"        ]"
				+"    },"
				+"    \"url\": \"http://www.google.com\","
				+"    \"icons\": ["
				+"        {"
				+"            \"id\": 1,"
				+"            \"name\": \"regApp\","
				+"            \"label\": \"Regular Application\","
				+"            \"img\": \"1\","
				+"            \"x\": 1,"
				+"            \"y\": 1,"
				+"            \"type\": 1"
				+"        }"
				+"    ]"
				+"}";
		instance.setData(JSOModel.fromJson(jsonData));
	}

	@Test
	public void testGetUrl() {
		assertEquals("http://www.google.com", getField(IconInfo.url));
	}

	@Test
	public void testGetIcons() {
		ArrayList<Icon> icons = getArrayField(IconInfo.icons);
		assertEquals(ArrayList.class, icons.getClass());
		assertEquals(1, icons.size());
		
		Icon theIcon = icons.get(0);
		assertEquals(Icon.class, theIcon.getClass());
	}
	
	@Test
	public void testGetRightClickMenu() {
		Menu menu = getField(IconInfo.rClickMenu);
		assertEquals(Menu.class, menu.getClass());
	}

}
