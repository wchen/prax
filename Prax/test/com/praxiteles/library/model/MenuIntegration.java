package com.praxiteles.library.model;

import java.util.ArrayList;

import org.junit.Test;

import com.praxiteles.library.ModelTestBase;

public class MenuIntegration extends ModelTestBase {

	@Override
	protected void preSetUp() {
		instance = new Menu();
	}
	
	@Override
	protected void loadData() {
		jsonData = "{"
				+"    \"menu_options\": ["
				+"        {"
				+"            \"name\": \"Show Desktop\","
				+"            \"event\": \"showDesktop\""
				+"        },"
				+"        {"
				+"            \"name\": \"New\","
				+"            \"event\": \"showMenuOptions\","
				+"            \"menu_options\": ["
				+"                {"
				+"                    \"name\": \"Application\","
				+"                    \"event\": \"addApp\","
				+"                    \"img\": \"newApp\""
				+"                },"
				+"                {"
				+"                    \"name\": \"Folder\","
				+"                    \"event\": \"addFolder\","
				+"                    \"img\": \"addFolder\""
				+"                }"
				+"            ]"
				+"        },"
				+"        {"
				+"            \"name\": \"Separator\""
				+"        },"
				+"        {"
				+"            \"name\": \"Change Background Image\","
				+"            \"event\": \"changeBgImg\""
				+"        },"
				+"        {"
				+"            \"name\": \"Log Off\","
				+"            \"event\": \"logOff\""
				+"        }"
				+"    ]"
				+"}";
		instance.setData(JSOModel.fromJson(jsonData));
	}

	@Test
	public void testMenuOptions() {
		ArrayList<MenuOption> menuOptions = getArrayField(Menu.menuOptions);
		assertEquals(ArrayList.class, menuOptions.getClass());
		assertEquals(5, menuOptions.size());
		
		MenuOption theMenuOption = menuOptions.get(0);
		assertEquals(MenuOption.class, theMenuOption.getClass());
	}
}
