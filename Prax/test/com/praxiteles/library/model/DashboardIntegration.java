package com.praxiteles.library.model;

import java.util.ArrayList;

import org.junit.Test;

import com.praxiteles.library.ModelTestBase;

public class DashboardIntegration extends ModelTestBase {

	@Override
	protected void preSetUp() {
		instance = new Dashboard();
	}
	
	@Override
	protected void loadData() {
		jsonData = "{"
				+"    \"current_workspace\": 1,"
				+"    \"workspaces\": ["
				+"        {"
				+"            \"id\": 1,"
				+"            \"bg_img\": \"bg1\","
				+"            \"rclick_menu\": {"
				+"                \"menu_options\": ["
				+"                    {"
				+"                        \"name\": \"Show Desktop\","
				+"                        \"event\": \"showDesktop\""
				+"                    },"
				+"                    {"
				+"                        \"name\": \"New\","
				+"                        \"event\": \"showMenuOptions\","
				+"                        \"menu_options\": ["
				+"                            {"
				+"                                \"name\": \"Application\","
				+"                                \"event\": \"addApp\","
				+"                                \"img\": \"newApp\""
				+"                            },"
				+"                            {"
				+"                                \"name\": \"Folder\","
				+"                                \"event\": \"addFolder\","
				+"                                \"img\": \"addFolder\""
				+"                            }"
				+"                        ]"
				+"                    },"
				+"                    {"
				+"                        \"name\": \"Separator\""
				+"                    },"
				+"                    {"
				+"                        \"name\": \"Change Background Image\","
				+"                        \"event\": \"changeBgImg\""
				+"                    },"
				+"                    {"
				+"                        \"name\": \"Log Off\","
				+"                        \"event\": \"logOff\""
				+"                    }"
				+"                ]"
				+"            },"
				+"            \"icons\": ["
				+"                {"
				+"                    \"id\": 1,"
				+"                    \"name\": \"regApp\","
				+"                    \"label\": \"Regular Application\","
				+"                    \"img\": \"1\","
				+"                    \"x\": 1,"
				+"                    \"y\": 1,"
				+"                    \"type\": 1"
				+"                },"
				+"                {"
				+"                    \"id\": 2,"
				+"                    \"name\": \"externalLink\","
				+"                    \"label\": \"Google\","
				+"                    \"img\": \"2\","
				+"                    \"x\": 1,"
				+"                    \"y\": 2,"
				+"                    \"type\": 2,"
				+"                    \"info\": {"
				+"                        \"url\": \"http://www.google.com\""
				+"                    }"
				+"                },"
				+"                {"
				+"                    \"id\": 3,"
				+"                    \"name\": \"sysApp\","
				+"                    \"label\": \"System Application with custom right click menu\","
				+"                    \"img\": \"3\","
				+"                    \"x\": 1,"
				+"                    \"y\": 3,"
				+"                    \"type\": 3,"
				+"                    \"info\": {"
				+"                        \"rclick_menu\": {"
				+"                            \"menu_options\": ["
				+"                                {"
				+"                                    \"name\": \"Launch Application\","
				+"                                    \"event\": \"launch\","
				+"                                    \"parameters\": ["
				+"                                        \"arg1\","
				+"                                        \"arg2\""
				+"                                    ]"
				+"                                },"
				+"                                {"
				+"                                    \"name\": \"Shake the earth\","
				+"                                    \"event\": \"shake\""
				+"                                },"
				+"                                {"
				+"                                    \"name\": \"Uninstall Application\","
				+"                                    \"event\": \"uninstall\","
				+"                                    \"parameters\": ["
				+"                                        \"arg1\","
				+"                                        \"arg2\""
				+"                                    ]"
				+"                                }"
				+"                            ]"
				+"                        }"
				+"                    }"
				+"                },"
				+"                {"
				+"                    \"id\": 4,"
				+"                    \"name\": \"folder\","
				+"                    \"label\": \"Folder\","
				+"                    \"img\": \"4\","
				+"                    \"x\": 1,"
				+"                    \"y\": 4,"
				+"                    \"type\": 4,"
				+"                    \"info\": {"
				+"                        \"icons\": ["
				+"                            {"
				+"                                \"id\": 1,"
				+"                                \"name\": \"regApp\","
				+"                                \"label\": \"Regular Application\","
				+"                                \"img\": \"1\","
				+"                                \"x\": 1,"
				+"                                \"y\": 1,"
				+"                                \"type\": 1"
				+"                            }"
				+"                        ]"
				+"                    }"
				+"                }"
				+"            ]"
				+"        }"
				+"    ]"
				+"}";
		instance.setData(JSOModel.fromJson(jsonData));
	}

	@Test
	public void testGetCurrentWorkspace() {
		assertEquals(1, getField(Dashboard.currentWorkspace));
	}

	@Test
	public void testGetWorkspaces() {
		ArrayList<Workspace> workspaces = getArrayField(Dashboard.workspaces);
		assertEquals(ArrayList.class, workspaces.getClass());
		assertEquals(1, workspaces.size());

		Workspace theWorkspace = workspaces.get(0);
		assertEquals(Workspace.class, theWorkspace.getClass());
	}

}
