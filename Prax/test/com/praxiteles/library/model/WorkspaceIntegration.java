package com.praxiteles.library.model;

import java.util.ArrayList;

import org.junit.Test;

import com.praxiteles.library.ModelTestBase;

public class WorkspaceIntegration extends ModelTestBase {

	@Override
	protected void preSetUp() {
		instance = new Workspace();
	}
	
	@Override
	protected void loadData() {
		jsonData = "{"
				+"    \"id\": 1,"
				+"    \"bg_img\": \"bg1\","
				+"    \"rclick_menu\": {"
				+"        \"menu_options\": ["
				+"            {"
				+"                \"name\": \"Show Desktop\","
				+"                \"event\": \"showDesktop\""
				+"            },"
				+"            {"
				+"                \"name\": \"New\","
				+"                \"event\": \"showMenuOptions\","
				+"                \"menu_options\": ["
				+"                    {"
				+"                        \"name\": \"Application\","
				+"                        \"event\": \"addApp\","
				+"                        \"img\": \"newApp\""
				+"                    },"
				+"                    {"
				+"                        \"name\": \"Folder\","
				+"                        \"event\": \"addFolder\","
				+"                        \"img\": \"addFolder\""
				+"                    }"
				+"                ]"
				+"            },"
				+"            {"
				+"                \"name\": \"Separator\""
				+"            },"
				+"            {"
				+"                \"name\": \"Change Background Image\","
				+"                \"event\": \"changeBgImg\""
				+"            },"
				+"            {"
				+"                \"name\": \"Log Off\","
				+"                \"event\": \"logOff\""
				+"            }"
				+"        ]"
				+"    },"
				+"    \"icons\": ["
				+"        {"
				+"            \"id\": 1,"
				+"            \"name\": \"regApp\","
				+"            \"label\": \"Regular Application\","
				+"            \"img\": \"1\","
				+"            \"x\": 1,"
				+"            \"y\": 1,"
				+"            \"type\": 1"
				+"        },"
				+"        {"
				+"            \"id\": 2,"
				+"            \"name\": \"externalLink\","
				+"            \"label\": \"Google\","
				+"            \"img\": \"2\","
				+"            \"x\": 1,"
				+"            \"y\": 2,"
				+"            \"type\": 2,"
				+"            \"info\": {"
				+"                \"url\": \"http://www.google.com\""
				+"            }"
				+"        },"
				+"        {"
				+"            \"id\": 3,"
				+"            \"name\": \"sysApp\","
				+"            \"label\": \"System Application with custom right click menu\","
				+"            \"img\": \"3\","
				+"            \"x\": 1,"
				+"            \"y\": 3,"
				+"            \"type\": 3,"
				+"            \"info\": {"
				+"                \"rclick_menu\": {"
				+"                    \"menu_options\": ["
				+"                        {"
				+"                            \"name\": \"Launch Application\","
				+"                            \"event\": \"launch\","
				+"                            \"parameters\": ["
				+"                                \"arg1\","
				+"                                \"arg2\""
				+"                            ]"
				+"                        },"
				+"                        {"
				+"                            \"name\": \"Shake the earth\","
				+"                            \"event\": \"shake\""
				+"                        },"
				+"                        {"
				+"                            \"name\": \"Uninstall Application\","
				+"                            \"event\": \"uninstall\","
				+"                            \"parameters\": ["
				+"                                \"arg1\","
				+"                                \"arg2\""
				+"                            ]"
				+"                        }"
				+"                    ]"
				+"                }"
				+"            }"
				+"        },"
				+"        {"
				+"            \"id\": 4,"
				+"            \"name\": \"folder\","
				+"            \"label\": \"Folder\","
				+"            \"img\": \"4\","
				+"            \"x\": 1,"
				+"            \"y\": 4,"
				+"            \"type\": 4,"
				+"            \"info\": {"
				+"                \"icons\": ["
				+"                    {"
				+"                        \"id\": 1,"
				+"                        \"name\": \"regApp\","
				+"                        \"label\": \"Regular Application\","
				+"                        \"img\": \"1\","
				+"                        \"x\": 1,"
				+"                        \"y\": 1,"
				+"                        \"type\": 1"
				+"                    }"
				+"                ]"
				+"            }"
				+"        }"
				+"    ]"
				+"}";
		instance.setData(JSOModel.fromJson(jsonData));
	}

	@Test
	public void testGetId() {
		assertEquals(1, getField(Workspace.id));
	}

	@Test
	public void testGetBgImg() {
		assertEquals("bg1", getField(Workspace.bgImg));
	}
	
	@Test
	public void testGetRightClickMenu() {
		Menu menu = getField(Workspace.rClickMenu);
		assertEquals(Menu.class, menu.getClass());
	}

	@Test
	public void testGetIcons() {
		ArrayList<Icon> icons = getArrayField(Workspace.icons);
		assertEquals(ArrayList.class, icons.getClass());
		assertEquals(4, icons.size());
		
		Icon theIcon = icons.get(0);
		assertEquals(Icon.class, theIcon.getClass());
	}
}
