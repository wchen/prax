package com.praxiteles.library;

import java.util.ArrayList;

import com.praxiteles.library.field.Field;
import com.praxiteles.library.model.ModelVirtualProxy;

public class ModelTestBase extends GWTTestCaseBase {

	protected ModelVirtualProxy instance;
	protected String jsonData = null;

	@Override
	public String getModuleName() {
		// TODO Auto-generated method stub
		return "com.praxiteles.Prax";
	}

	protected <T> T getField(Field key) {
		return instance.get(key);
	}

	protected <T> T getField(ModelVirtualProxy model, Field key) {
		return model.get(key);
	}

	protected <T> ArrayList<T> getArrayField(Field key) {
		return instance.getArray(key);
	}

	protected <T> ArrayList<T> getArrayField(ModelVirtualProxy model, Field key) {
		return model.getArray(key);
	}

}
