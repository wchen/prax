package com.praxiteles.historyConverter;

import com.mvp4g.client.annotation.History;
import com.mvp4g.client.annotation.History.HistoryConverterType;
import com.mvp4g.client.history.HistoryConverter;
import com.praxiteles.module.main.MainEventBus;

@History(type = HistoryConverterType.NONE)
public class LoginHistoryConverter implements HistoryConverter<MainEventBus> {

	public void convertFromToken(String historyName, String param,
			MainEventBus eventBus) {
		eventBus.login();
	}

	public boolean isCrawlable() {
		return true;
	}
}
