package com.praxiteles.historyConverter;

import com.mvp4g.client.annotation.History;
import com.mvp4g.client.annotation.History.HistoryConverterType;
import com.mvp4g.client.history.HistoryConverter;
import com.praxiteles.module.dashboard.DashboardEventBus;

@History(type = HistoryConverterType.NONE)
public class DashboardHistoryConverter implements
		HistoryConverter<DashboardEventBus> {

	public void convertFromToken(String historyName, String param,
			DashboardEventBus eventBus) {
		eventBus.dashboard();
	}

	public boolean isCrawlable() {
		return false;
	}
}
