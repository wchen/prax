package com.praxiteles.historyConverter;

import com.mvp4g.client.annotation.History;
import com.mvp4g.client.annotation.History.HistoryConverterType;
import com.mvp4g.client.history.HistoryConverter;
import com.praxiteles.module.widgetWindow.WidgetWindowEventBus;

@History(type = HistoryConverterType.NONE)
public class FileBrowserHistoryConverter implements
		HistoryConverter<WidgetWindowEventBus> {

	public void convertFromToken(String historyName, String param,
			WidgetWindowEventBus eventBus) {
		eventBus.fileBrowser();
	}

	public boolean isCrawlable() {
		return false;
	}
}
