package com.praxiteles.resource;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.praxiteles.module.main.presenter.interfaces.ILandingPageView.LandingPageCss;

public interface LandingPageResource extends ClientBundle {
	public static final LandingPageResource INSTANCE =  GWT.create(LandingPageResource.class);
	
	@Source("com/praxiteles/resource/image/blog-black.png")
	ImageResource blogBlack();

	@Source("com/praxiteles/resource/image/blog-blue.png")
	ImageResource blogBlue();

	@Source("com/praxiteles/resource/image/company.png")
	ImageResource company();

	@Source("com/praxiteles/resource/image/connect.png")
	ImageResource connect();

	@Source("com/praxiteles/resource/image/home-black.png")
	ImageResource homeBlack();

	@Source("com/praxiteles/resource/image/home-blue.png")
	ImageResource homeBlue();

	@Source("com/praxiteles/resource/image/legal.png")
	ImageResource legal();

	@Source("com/praxiteles/resource/image/moondog-logo-1.png")
	ImageResource moondogLogo1();

	@Source("com/praxiteles/resource/image/my-moondog-black.png")
	ImageResource myMoondogBlack();

	@Source("com/praxiteles/resource/image/my-moondog-blue.png")
	ImageResource myMoondogBlue();

	@Source("com/praxiteles/resource/image/projects.png")
	ImageResource projects();

	@Source("com/praxiteles/resource/css/landing-page.css")
	LandingPageCss landingPageCss();

}
