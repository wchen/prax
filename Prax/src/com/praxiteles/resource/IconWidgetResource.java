package com.praxiteles.resource;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.praxiteles.library.widget.IconWidget.IconWidgetCss;

public interface IconWidgetResource extends ClientBundle {
	public static final IconWidgetResource INSTANCE = GWT
			.create(IconWidgetResource.class);

	@Source("com/praxiteles/resource/css/iconWidget.css")
	IconWidgetCss iconWidgetCss();

}
