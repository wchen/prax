package com.praxiteles.module.widgetWindow.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class WidgetHeaderView extends Composite {

	interface FileBrowserHeaderWidgetUiBinder extends
			UiBinder<Widget, WidgetHeaderView> {
	}

	private static FileBrowserHeaderWidgetUiBinder uiBinder = GWT
			.create(FileBrowserHeaderWidgetUiBinder.class);
	@UiField
	FlowPanel control;
	@UiField
	FlowPanel windowButtons;
	@UiField
	Image homeLink;
	@UiField
	Image shareLink;
	@UiField
	Image ratingLink;
	@UiField
	Image commentLink;
	@UiField
	Label title;
	@UiField
	Image minimize;
	@UiField
	Image maximize;

	@UiField
	Image close;

	public WidgetHeaderView() {
		this.initWidget(uiBinder.createAndBindUi(this));

		this.setTitle("File Browser");
	}

	@UiHandler("close")
	void handleClick(ClickEvent e) {
		Window.alert("You can checkout any time you like, but you can never leave! ");
	}

	@Override
	public void setTitle(String name) {
		this.title.setText(name);
	}

}
