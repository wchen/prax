package com.praxiteles.module.widgetWindow.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.Widget;

public class WidgetWindowView extends ResizeComposite {

	interface WidgetWindowViewUiBinder extends
			UiBinder<Widget, WidgetWindowView> {
	}

	private static WidgetWindowViewUiBinder uiBinder = GWT
			.create(WidgetWindowViewUiBinder.class);

	public WidgetWindowView() {
		this.initWidget(uiBinder.createAndBindUi(this));
	}

}
