package com.praxiteles.module.widgetWindow.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.Widget;

public class FileBrowserView extends ResizeComposite {

	interface FileBrowserViewUiBinder extends UiBinder<Widget, FileBrowserView> {
	}

	private static FileBrowserViewUiBinder uiBinder = GWT
			.create(FileBrowserViewUiBinder.class);

	@UiField
	ShortcutListView groups, personal;

	public FileBrowserView() {
		this.initWidget(uiBinder.createAndBindUi(this));
		this.groups.setShortcutGroupImages();
		this.personal.setShortcutGroupImages();
	}

}
