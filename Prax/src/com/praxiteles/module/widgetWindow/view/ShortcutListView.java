package com.praxiteles.module.widgetWindow.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

public class ShortcutListView extends Composite {

	interface ShortcutListWidgetUiBinder extends
			UiBinder<Widget, ShortcutListView> {
	}

	private static ShortcutListWidgetUiBinder uiBinder = GWT
			.create(ShortcutListWidgetUiBinder.class);

	@UiField
	Image shortcutGroupNameIcon, shortcutGroupNameImage;

	public ShortcutListView() {
		this.initWidget(uiBinder.createAndBindUi(this));
	}

	public void setShortcutGroupImages() {
		String title = this.getTitle();
		this.shortcutGroupNameIcon.setUrl("prax/image/shortcut_list/" + title
				+ "-icon.png");
		this.shortcutGroupNameImage.setUrl("prax/image/shortcut_list/" + title
				+ "-name.png");
	}

}
