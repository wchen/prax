package com.praxiteles.module.widgetWindow;

import com.google.gwt.user.client.ui.Composite;
import com.mvp4g.client.annotation.Debug;
import com.mvp4g.client.annotation.Debug.LogLevel;
import com.mvp4g.client.annotation.Event;
import com.mvp4g.client.annotation.Events;
import com.mvp4g.client.event.EventBus;
import com.praxiteles.historyConverter.FileBrowserHistoryConverter;
import com.praxiteles.module.widgetWindow.presenter.WidgetWindowPresenter;
import com.praxiteles.module.widgetWindow.view.WidgetWindowView;

@Debug(logLevel = LogLevel.DETAILED)
@Events(startView = WidgetWindowView.class, module = WidgetWindowModule.class)
public interface WidgetWindowEventBus extends EventBus {
	@Event(handlers = WidgetWindowPresenter.class, historyConverter = FileBrowserHistoryConverter.class)
	public void fileBrowser();

	@Event(forwardToParent = true)
	public void setView(Composite composite);
}
