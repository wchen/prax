package com.praxiteles.module.widgetWindow.presenter;

import com.google.gwt.core.client.GWT;
import com.mvp4g.client.annotation.Presenter;
import com.mvp4g.client.presenter.BasePresenter;
import com.praxiteles.module.widgetWindow.WidgetWindowEventBus;
import com.praxiteles.module.widgetWindow.view.WidgetWindowView;

@Presenter(view = WidgetWindowView.class)
public class WidgetWindowPresenter extends
		BasePresenter<WidgetWindowView, WidgetWindowEventBus> {
	public void onFileBrowser() {
		GWT.log("WidgetWindowPresenter: Loads fileBrowser widget");
		this.eventBus.setView(view);
	}
}
