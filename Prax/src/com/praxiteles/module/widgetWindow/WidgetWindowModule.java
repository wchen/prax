package com.praxiteles.module.widgetWindow;

import com.mvp4g.client.Mvp4gModule;
import com.mvp4g.client.annotation.module.HistoryName;

@HistoryName("WidgetWindow")
public interface WidgetWindowModule extends Mvp4gModule {

}
