package com.praxiteles.module.window;

import com.google.gwt.touch.client.Point;
import com.google.gwt.user.client.ui.Composite;

public interface WindowViewInterface {

	public int getHeight();

	public Point getPosition();

	public String getTitle();

	public int getWidth();

	public int getX();

	// public void setTheme(Theme theme);
	// public void getThemeTitle();

	public int getY();

	public int getZIndex();

	public void setBody(Composite body);

	public void setHeight(String height);

	public void setPosition(Point position);

	public void setTitle(String title);

	public void setVisible(Boolean visibility);

	public void setWidth(String width);

	public void setX(int x);

	public void setY(int y);

	public void setZIndex(int zindex);

}
