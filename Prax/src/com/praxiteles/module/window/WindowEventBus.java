package com.praxiteles.module.window;

import com.mvp4g.client.annotation.Event;
import com.mvp4g.client.annotation.Events;
import com.mvp4g.client.event.EventBus;

@Events(startView = WindowView.class, module = WindowModule.class)
public interface WindowEventBus extends EventBus {
	@Event(handlers = WindowPresenter.class)
	public void goToWindow();

	@Event(forwardToParent = true)
	public void setBody(String message);

}
