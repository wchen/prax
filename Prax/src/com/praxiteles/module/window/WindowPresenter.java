package com.praxiteles.module.window;

import com.google.gwt.touch.client.Point;
import com.mvp4g.client.annotation.Presenter;
import com.mvp4g.client.presenter.BasePresenter;

@Presenter(view = WindowView.class)
public class WindowPresenter extends
		BasePresenter<WindowViewInterface, WindowEventBus> {
	enum WindowState {
		INITIALIZING, CURRENT, MINIMIZED, MAXIMIZED, CLOSED
	}

	private int windowId = -1;
	private WindowState windowState;

	public void close() {
	}

	public void current() {
	}

	// index number windows array
	public int getWindowId() {
		return this.windowId;
	}

	public WindowState getWindowState() {
		return this.windowState;
	}

	public void maximize() {
	}

	public void minimize() {
	}

	public void moveToPoint(Point toPoint) {
	}

	public void onGoToWindow() {
	}

	public void resize() {
	}

	public void setWindowState(WindowState newWindowState) {
		this.windowState = newWindowState;
	}
}
