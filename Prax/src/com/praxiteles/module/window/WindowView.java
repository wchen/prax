package com.praxiteles.module.window;

import com.google.gwt.core.client.GWT;
import com.google.gwt.touch.client.Point;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class WindowView extends Composite implements WindowViewInterface {
	interface WindowViewUiBinder extends UiBinder<Widget, WindowView> {
	}

	// private Point point;
	private static WindowViewUiBinder uiBinder = GWT
			.create(WindowViewUiBinder.class);

	@UiField
	HTMLPanel window;

	public WindowView() {
		this.initWidget(uiBinder.createAndBindUi(this));
	}

	public int getHeight() {
		// TODO Auto-generated method stub
		return 0;
	}

	public Point getPosition() {
		// TODO Auto-generated method stub
		return null;
	}

	public int getWidth() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getX() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getY() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getZIndex() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setBody(Composite body) {
		// TODO Auto-generated method stub

	}

	public void setPosition(Point position) {
		// TODO Auto-generated method stub

	}

	public void setVisible(Boolean visibility) {
		// TODO Auto-generated method stub

	}

	public void setX(int x) {
		// TODO Auto-generated method stub

	}

	public void setY(int y) {
		// TODO Auto-generated method stub

	}

	public void setZIndex(int zindex) {
		// TODO Auto-generated method stub

	}

}
