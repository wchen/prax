package com.praxiteles.module.dashboard;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.mvp4g.client.annotation.Debug;
import com.mvp4g.client.annotation.Debug.LogLevel;
import com.mvp4g.client.annotation.Event;
import com.mvp4g.client.annotation.Events;
import com.mvp4g.client.event.EventBusWithLookup;
import com.praxiteles.historyConverter.DashboardHistoryConverter;
import com.praxiteles.library.model.Workspace;
import com.praxiteles.module.dashboard.presenter.DashboardPresenter;
import com.praxiteles.module.dashboard.presenter.TaskbarPresenter;
import com.praxiteles.module.dashboard.presenter.TopbarPresenter;
import com.praxiteles.module.dashboard.presenter.WorkspacePresenter;
import com.praxiteles.module.dashboard.view.DashboardView;

@Debug(logLevel = LogLevel.SIMPLE)
@Events(startView = DashboardView.class, module = DashboardModule.class)
public interface DashboardEventBus extends EventBusWithLookup {

	@Event(handlers = DashboardPresenter.class, historyConverter = DashboardHistoryConverter.class)
	public void dashboard();

	@Event(handlers = { DashboardPresenter.class, TopbarPresenter.class,
			TaskbarPresenter.class, WorkspacePresenter.class })
	public void initDashboard();

	@Event(handlers = DashboardPresenter.class)
	public void setDashboardTopbar(Widget widget);

	@Event(handlers = DashboardPresenter.class)
	public void setDashboardTaskbar(Widget widget);

	@Event(handlers = DashboardPresenter.class)
	public void setDashboardWorkspace(Widget widget);

	@Event(forwardToParent = true)
	public void setView(Composite composite);

	@Event(handlers = DashboardPresenter.class)
	public void loadData();
	
	@Event(handlers = WorkspacePresenter.class)
	public void setWorkspaceData(Workspace data);
	
	@Event(handlers = WorkspacePresenter.class)
	public void openRegApp();
	
}
