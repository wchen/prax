package com.praxiteles.module.dashboard.presenter.interfaces;

import com.mvp4g.client.view.ReverseViewInterface;

public interface ITopbarView extends
		ReverseViewInterface<ITopbarView.ITopbarPresenter> {

	interface ITopbarPresenter {
	};

}
