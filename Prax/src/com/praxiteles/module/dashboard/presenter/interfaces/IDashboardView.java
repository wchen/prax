package com.praxiteles.module.dashboard.presenter.interfaces;

import com.mvp4g.client.view.ReverseViewInterface;

public interface IDashboardView extends
		ReverseViewInterface<IDashboardView.IDashboardPresenter> {

	interface IDashboardPresenter {
	};

}
