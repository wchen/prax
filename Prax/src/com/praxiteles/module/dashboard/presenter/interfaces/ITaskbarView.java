package com.praxiteles.module.dashboard.presenter.interfaces;

import com.mvp4g.client.view.ReverseViewInterface;

public interface ITaskbarView extends
		ReverseViewInterface<ITaskbarView.ITaskbarPresenter> {

	interface ITaskbarPresenter {
	};

}
