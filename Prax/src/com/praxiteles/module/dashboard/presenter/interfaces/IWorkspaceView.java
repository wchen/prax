package com.praxiteles.module.dashboard.presenter.interfaces;

import com.mvp4g.client.view.ReverseViewInterface;
import com.praxiteles.library.model.Menu;
import com.praxiteles.library.widget.IconWidget;

public interface IWorkspaceView extends
		ReverseViewInterface<IWorkspaceView.IWorkspacePresenter> {

	interface IWorkspacePresenter {
	};

	public void setBackgroundImage(String bgImg);

	public void setRightClickMenu(Menu rightClickMent);

	public void addIconWidget(IconWidget icon);

}
