package com.praxiteles.module.dashboard.presenter;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;
import com.mvp4g.client.annotation.Presenter;
import com.mvp4g.client.presenter.LazyPresenter;
import com.praxiteles.library.model.Dashboard;
import com.praxiteles.library.model.JSOModel;
import com.praxiteles.library.model.Workspace;
import com.praxiteles.module.dashboard.DashboardEventBus;
import com.praxiteles.module.dashboard.view.DashboardView;

@Presenter(view = DashboardView.class)
public class DashboardPresenter extends
		LazyPresenter<DashboardView, DashboardEventBus> implements
		DashboardView.IDashboardPresenter {
	private Dashboard instance;

	public void onSetDashboardTopbar(Widget widget) {
		view.setTopbar(widget);
	}

	public void onSetDashboardTaskbar(Widget widget) {
		view.setTaskbar(widget);
	}

	public void onSetDashboardWorkspace(Widget widget) {
		view.setWorkspace(widget);
	}

	private String getData() {
		String jsonData = "{"
				+ "    \"current_workspace\": 0,"
				+ "    \"workspaces\": ["
				+ "        {"
				+ "            \"id\": 1,"
				+ "            \"bg_img\": \"bg1\","
				+ "            \"rclick_menu\": {"
				+ "                \"menu_options\": ["
				+ "                    {"
				+ "                        \"name\": \"Show Desktop\","
				+ "                        \"event\": \"showDesktop\""
				+ "                    },"
				+ "                    {"
				+ "                        \"name\": \"New\","
				+ "                        \"event\": \"showMenuOptions\","
				+ "                        \"menu_options\": ["
				+ "                            {"
				+ "                                \"name\": \"Application\","
				+ "                                \"event\": \"addApp\","
				+ "                                \"img\": \"newApp\""
				+ "                            },"
				+ "                            {"
				+ "                                \"name\": \"Folder\","
				+ "                                \"event\": \"addFolder\","
				+ "                                \"img\": \"addFolder\""
				+ "                            }"
				+ "                        ]"
				+ "                    },"
				+ "                    {"
				+ "                        \"name\": \"Separator\""
				+ "                    },"
				+ "                    {"
				+ "                        \"name\": \"Change Background Image\","
				+ "                        \"event\": \"changeBgImg\""
				+ "                    },"
				+ "                    {"
				+ "                        \"name\": \"Log Off\","
				+ "                        \"event\": \"logOff\""
				+ "                    }"
				+ "                ]"
				+ "            },"
				+ "            \"icons\": ["
				+ "                {"
				+ "                    \"id\": 1,"
				+ "                    \"name\": \"regApp\","
				+ "                    \"label\": \"Regular Application\","
				+ "                    \"img\": \"1\","
				+ "                    \"x\": 1,"
				+ "                    \"y\": 1,"
				+ "                    \"type\": 1"
				+ "                },"
				+ "                {"
				+ "                    \"id\": 2,"
				+ "                    \"name\": \"externalLink\","
				+ "                    \"label\": \"Google\","
				+ "                    \"img\": \"2\","
				+ "                    \"x\": 1,"
				+ "                    \"y\": 2,"
				+ "                    \"type\": 2,"
				+ "                    \"auxiliary\": {"
				+ "                        \"url\": \"http://www.google.com\""
				+ "                    }"
				+ "                },"
				+ "                {"
				+ "                    \"id\": 3,"
				+ "                    \"name\": \"sysApp\","
				+ "                    \"label\": \"System Application with custom right click menu\","
				+ "                    \"img\": \"3\","
				+ "                    \"x\": 1,"
				+ "                    \"y\": 3,"
				+ "                    \"type\": 3,"
				+ "                    \"auxiliary\": {"
				+ "                        \"rclick_menu\": {"
				+ "                            \"menu_options\": ["
				+ "                                {"
				+ "                                    \"name\": \"Launch Application\","
				+ "                                    \"event\": \"launch\","
				+ "                                    \"parameters\": ["
				+ "                                        \"arg1\","
				+ "                                        \"arg2\""
				+ "                                    ]"
				+ "                                },"
				+ "                                {"
				+ "                                    \"name\": \"Shake the earth\","
				+ "                                    \"event\": \"shake\""
				+ "                                },"
				+ "                                {"
				+ "                                    \"name\": \"Uninstall Application\","
				+ "                                    \"event\": \"uninstall\","
				+ "                                    \"parameters\": ["
				+ "                                        \"arg1\","
				+ "                                        \"arg2\""
				+ "                                    ]"
				+ "                                }"
				+ "                            ]"
				+ "                        }"
				+ "                    }"
				+ "                },"
				+ "                {"
				+ "                    \"id\": 4,"
				+ "                    \"name\": \"folder\","
				+ "                    \"label\": \"Folder\","
				+ "                    \"img\": \"4\","
				+ "                    \"x\": 1,"
				+ "                    \"y\": 4,"
				+ "                    \"type\": 4,"
				+ "                    \"auxiliary\": {"
				+ "                        \"icons\": ["
				+ "                            {"
				+ "                                \"id\": 1,"
				+ "                                \"name\": \"regApp\","
				+ "                                \"label\": \"Regular Application\","
				+ "                                \"img\": \"1\","
				+ "                                \"x\": 1,"
				+ "                                \"y\": 1,"
				+ "                                \"type\": 1"
				+ "                            }" 
				+ "                        ]"
				+ "                    }" 
				+ "                }"
				+ "            ]" 
				+ "        }" 
				+ "    ]" 
				+ "}";
		return jsonData;
	}

	public void onInitDashboard() {
		eventBus.setView(view);
	}

	public void onLoadData() {
		instance = new Dashboard();
		instance.setData(JSOModel.fromJson(getData()));
		int currentWorkspace = instance.get(Dashboard.currentWorkspace);
		ArrayList<Workspace> workspaces = instance
				.getArray(Dashboard.workspaces);
		if (workspaces != null && workspaces.size() > currentWorkspace) {
			eventBus.setWorkspaceData(workspaces.get(currentWorkspace));
		} else {
			GWT.log("Unable to fetch data.");
		}
	}

	public void onDashboard() {
		eventBus.initDashboard();
		eventBus.loadData();
	}
}