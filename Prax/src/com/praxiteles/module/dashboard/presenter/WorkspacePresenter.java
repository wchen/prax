package com.praxiteles.module.dashboard.presenter;

import static com.google.gwt.query.client.GQuery.$;
import static gwtquery.plugins.droppable.client.Droppable.Droppable;
import gwtquery.plugins.droppable.client.DroppableOptions;
import gwtquery.plugins.droppable.client.DroppableOptions.DroppableFunction;
import gwtquery.plugins.droppable.client.events.DragAndDropContext;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.mvp4g.client.annotation.Presenter;
import com.mvp4g.client.presenter.LazyPresenter;
import com.praxiteles.library.AppConfiguration;
import com.praxiteles.library.model.Icon;
import com.praxiteles.library.model.Menu;
import com.praxiteles.library.model.Workspace;
import com.praxiteles.library.widget.IconWidget;
import com.praxiteles.module.dashboard.DashboardEventBus;
import com.praxiteles.module.dashboard.view.WorkspaceView;

@Presenter(view = WorkspaceView.class)
public class WorkspacePresenter extends
		LazyPresenter<WorkspaceView, DashboardEventBus> implements
		WorkspaceView.IWorkspacePresenter {
	private Workspace instance;
	private HashMap<String, IconWidget> icons;
//	private ArrayList<Application> apps;
	
	public void onInitDashboard() {
		eventBus.setDashboardWorkspace(view);
		icons = new HashMap<String, IconWidget>();

		DroppableOptions options = new DroppableOptions();
		options.setAccept(DroppableOptions.ACCEPT_ALL);
		options.setOnDrop(new DroppableFunction() {

			public void f(DragAndDropContext context) {
				IconWidget iconWidget = (IconWidget) context
						.getDraggableWidget();
				float left = context.getHelperPosition().left;
				float top = context.getHelperPosition().top;
				int x = Math.round(left / AppConfiguration.ICON_X_WIDTH);
				int y = Math.round(top / AppConfiguration.ICON_Y_WIDTH);
				String position = x + "," + y;
				if (icons.containsKey(position)) {
					iconWidget.setRevertDuration(500);
				} else {
					icons.remove(iconWidget.getPosition());
					iconWidget.setRevertDuration(0);
					iconWidget.setPosition(x, y);
					icons.put(position, iconWidget);
				}
			}
		});
		$(".workspace").as(Droppable).droppable(options);

	}

	public void onSetWorkspaceData(Workspace data) {
		instance = data;

		String bgImg = AppConfiguration.IMAGE_BASEURL
				+ instance.get(Workspace.bgImg);
		view.setBackgroundImage(bgImg);

		Menu theMenu = instance.get(Workspace.rClickMenu);
		view.setRightClickMenu(theMenu);

		ArrayList<Icon> icons = instance.getArray(Workspace.icons);
		setIcons(icons);
	}

	public void setIcons(ArrayList<Icon> icons) {
		for (Icon theIcon : icons) {
			setIcon(theIcon);
		}
	}

	private void setIcon(Icon icon) {
		final IconWidget iconWidget = new IconWidget(icon);
		iconWidget.setEventBus(eventBus);
		icons.put(icon.getPositionString(), iconWidget);
		view.addIconWidget(iconWidget);
	}
	
	public void onOpenRegApp() {
		GWT.log("yayayay!");
	}
}
