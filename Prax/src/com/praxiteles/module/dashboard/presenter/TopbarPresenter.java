package com.praxiteles.module.dashboard.presenter;

import com.mvp4g.client.annotation.Presenter;
import com.mvp4g.client.presenter.LazyPresenter;
import com.praxiteles.module.dashboard.DashboardEventBus;
import com.praxiteles.module.dashboard.view.TopbarView;

@Presenter(view = TopbarView.class)
public class TopbarPresenter extends
		LazyPresenter<TopbarView, DashboardEventBus> implements
		TopbarView.ITopbarPresenter {
	public void onInitDashboard() {
		eventBus.setDashboardTopbar(view);
	}

}
