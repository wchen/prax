package com.praxiteles.module.dashboard.presenter;

import com.mvp4g.client.annotation.Presenter;
import com.mvp4g.client.presenter.LazyPresenter;
import com.praxiteles.module.dashboard.DashboardEventBus;
import com.praxiteles.module.dashboard.view.TaskbarView;

@Presenter(view = TaskbarView.class)
public class TaskbarPresenter extends
		LazyPresenter<TaskbarView, DashboardEventBus> implements
		TaskbarView.ITaskbarPresenter {
	public void onInitDashboard() {
		eventBus.setDashboardTaskbar(view);
	}

}
