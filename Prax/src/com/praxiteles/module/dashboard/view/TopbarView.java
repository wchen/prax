package com.praxiteles.module.dashboard.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Singleton;
import com.praxiteles.library.widget.LazyReverseComposite;
import com.praxiteles.module.dashboard.presenter.interfaces.ITopbarView;
import com.praxiteles.module.dashboard.presenter.interfaces.ITopbarView.ITopbarPresenter;

@Singleton
public class TopbarView extends LazyReverseComposite<ITopbarPresenter>
		implements ITopbarView {
	private static TopbarViewUiBinder uiBinder;

	interface TopbarViewUiBinder extends UiBinder<Widget, TopbarView> {
	}

	public void createView() {
		uiBinder = GWT.create(TopbarViewUiBinder.class);
		initWidget(uiBinder.createAndBindUi(this));

	}

}
