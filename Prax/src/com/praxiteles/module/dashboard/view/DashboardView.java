package com.praxiteles.module.dashboard.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Singleton;
import com.praxiteles.library.widget.LazyReverseResizeComposite;
import com.praxiteles.module.dashboard.presenter.interfaces.IDashboardView;
import com.praxiteles.module.dashboard.presenter.interfaces.IDashboardView.IDashboardPresenter;

@Singleton
public class DashboardView extends
		LazyReverseResizeComposite<IDashboardPresenter> implements
		IDashboardView {

	private static DashboardViewUiBinder uiBinder;

	@UiField
	SimplePanel topbar, taskbar, workspace;

	interface DashboardViewUiBinder extends UiBinder<Widget, DashboardView> {
	}

	public void createView() {
		uiBinder = GWT.create(DashboardViewUiBinder.class);
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void onAttach() {
		super.onAttach();
	}

	public void setTopbar(Widget widget) {
		topbar.setWidget(widget);
	}

	public void setTaskbar(Widget widget) {
		taskbar.setWidget(widget);
	}

	public void setWorkspace(Widget widget) {
		workspace.setWidget(widget);
	}

}
