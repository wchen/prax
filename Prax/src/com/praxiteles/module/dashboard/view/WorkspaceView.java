package com.praxiteles.module.dashboard.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Singleton;
import com.praxiteles.library.model.Menu;
import com.praxiteles.library.widget.IconWidget;
import com.praxiteles.library.widget.LazyReverseComposite;
import com.praxiteles.module.dashboard.presenter.interfaces.IWorkspaceView;
import com.praxiteles.module.dashboard.presenter.interfaces.IWorkspaceView.IWorkspacePresenter;

@Singleton
public class WorkspaceView extends LazyReverseComposite<IWorkspacePresenter>
		implements IWorkspaceView {
	private static WorkspaceViewUiBinder uiBinder;

	interface WorkspaceViewUiBinder extends UiBinder<Widget, WorkspaceView> {
	}

	@UiField
	ComplexPanel icons;

	// interface WorkspaceCss extends CssResource {
	// String bgImg();
	// }
	//
	public void createView() {
		uiBinder = GWT.create(WorkspaceViewUiBinder.class);
		initWidget(uiBinder.createAndBindUi(this));

	}

	public void setBackgroundImage(String bgImg) {
		getElement().getStyle().setProperty("backgroundImage", bgImg);
	}

	public void setRightClickMenu(Menu rightClickMent) {

	}

	public void addIconWidget(IconWidget icon) {
		icons.add(icon);
	}

}
