package com.praxiteles.module.dashboard.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Singleton;
import com.praxiteles.library.widget.LazyReverseComposite;
import com.praxiteles.module.dashboard.presenter.interfaces.ITaskbarView;
import com.praxiteles.module.dashboard.presenter.interfaces.ITaskbarView.ITaskbarPresenter;

@Singleton
public class TaskbarView extends LazyReverseComposite<ITaskbarPresenter>
		implements ITaskbarView {

	private static TaskbarViewUiBinder uiBinder;

	interface TaskbarViewUiBinder extends UiBinder<Widget, TaskbarView> {
	}

	public void createView() {
		uiBinder = GWT.create(TaskbarViewUiBinder.class);
		initWidget(uiBinder.createAndBindUi(this));
	}

}
