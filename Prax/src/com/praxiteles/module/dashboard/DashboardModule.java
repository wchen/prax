package com.praxiteles.module.dashboard;

import com.mvp4g.client.Mvp4gModule;
import com.mvp4g.client.annotation.module.HistoryName;

@HistoryName("dashboard")
public interface DashboardModule extends Mvp4gModule {

}
