package com.praxiteles.module.main.presenter;

import com.mvp4g.client.annotation.Presenter;
import com.mvp4g.client.presenter.LazyPresenter;
import com.praxiteles.library.Constant;
import com.praxiteles.module.main.MainEventBus;
import com.praxiteles.module.main.view.HeaderView;

@Presenter(view = HeaderView.class)
public class HeaderPresenter extends LazyPresenter<HeaderView, MainEventBus>
		implements HeaderView.IHeaderPresenter {

	public void goToLogin() {
		eventBus.login();
	}

	public void goToSlider() {
		eventBus.homepage();
	}

	public void onInitLandingPage(String param) {
		view.toggleHomeImage(false);
		view.toggleBlogImage(false);
		view.toggleMyMoondogImage(false);

		if (param.equals(Constant.SLIDER)) {
			view.toggleHomeImage(true);
		} else if (param.equals(Constant.BLOG)) {
			view.toggleBlogImage(true);
		} else if (param.equals(Constant.LOGIN) || param.equals(Constant.SIGNUP)) {
			view.toggleMyMoondogImage(true);
		}

		eventBus.setLandingPageHeader(view);
	}

}
