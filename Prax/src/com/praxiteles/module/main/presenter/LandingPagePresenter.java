package com.praxiteles.module.main.presenter;

import com.google.gwt.user.client.ui.Widget;
import com.mvp4g.client.annotation.Presenter;
import com.mvp4g.client.presenter.LazyPresenter;
import com.praxiteles.module.main.MainEventBus;
import com.praxiteles.module.main.view.LandingPageView;

@Presenter(view = LandingPageView.class)
public class LandingPagePresenter extends
		LazyPresenter<LandingPageView, MainEventBus> implements
		LandingPageView.ILandingPagePresenter {

	public void onGoToLandingPage() {
		eventBus.setView(view);
	}

	public void goToLogin() {
		eventBus.login();
	}

	public void onSetLandingPageBody(Widget widget) {
		view.setBody(widget);
	}

	public void onSetLandingPageHeader(Widget widget) {
		view.setHeader(widget);
	}

	public void onInitLandingPage(String param) {
		eventBus.setView(view);
	}
}
