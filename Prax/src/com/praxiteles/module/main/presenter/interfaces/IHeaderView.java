package com.praxiteles.module.main.presenter.interfaces;

import com.mvp4g.client.view.ReverseViewInterface;

public interface IHeaderView extends
		ReverseViewInterface<IHeaderView.IHeaderPresenter> {

	interface IHeaderPresenter {
		public void goToLogin();

		public void goToSlider();
	};

}
