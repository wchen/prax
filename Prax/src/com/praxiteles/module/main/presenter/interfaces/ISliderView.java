package com.praxiteles.module.main.presenter.interfaces;

import com.mvp4g.client.view.ReverseViewInterface;

public interface ISliderView extends
		ReverseViewInterface<ISliderView.ISliderPresenter> {

	interface ISliderPresenter {
	};

}
