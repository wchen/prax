package com.praxiteles.module.main.presenter.interfaces;

import com.mvp4g.client.view.ReverseViewInterface;

public interface IMyMoondogView extends
		ReverseViewInterface<IMyMoondogView.IMyMoondogPresenter> {

	interface IMyMoondogPresenter {
		public void goToSignup();

		public void goToLogin();
	};

}
