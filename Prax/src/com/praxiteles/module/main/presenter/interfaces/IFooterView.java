package com.praxiteles.module.main.presenter.interfaces;

import com.mvp4g.client.view.ReverseViewInterface;

public interface IFooterView extends
		ReverseViewInterface<IFooterView.IFooterPresenter> {

	interface IFooterPresenter {
	};

}
