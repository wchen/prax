package com.praxiteles.module.main.presenter.interfaces;

import com.google.gwt.user.client.ui.ProvidesResize;
import com.google.gwt.user.client.ui.Widget;
import com.mvp4g.client.view.LazyView;

public interface IMainView extends LazyView, ProvidesResize {
	public void setWidget(Widget widget);

	public void setStyleName(String style);

}