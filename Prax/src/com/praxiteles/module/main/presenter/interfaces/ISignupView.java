package com.praxiteles.module.main.presenter.interfaces;

import com.mvp4g.client.view.ReverseViewInterface;

public interface ISignupView extends
		ReverseViewInterface<ISignupView.ISignupPresenter> {

	interface ISignupPresenter {
		public void signup(String recaptchaChallenge, String recaptchaResponse, String firstName, String lastName, String email,
				String altEmail, String password, String confirmPassword,
				boolean terms);
	};

}
