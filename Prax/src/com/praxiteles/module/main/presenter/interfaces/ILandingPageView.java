package com.praxiteles.module.main.presenter.interfaces;

import com.google.gwt.resources.client.CssResource;
import com.mvp4g.client.view.ReverseViewInterface;

public interface ILandingPageView extends
		ReverseViewInterface<ILandingPageView.ILandingPagePresenter> {

	interface ILandingPagePresenter {
		public void goToLogin();
	};

	public interface LandingPageCss extends CssResource {
		String moondogLogo1();
	}
}
