package com.praxiteles.module.main.presenter.interfaces;

import com.mvp4g.client.view.ReverseViewInterface;

public interface ILoginView extends
		ReverseViewInterface<ILoginView.ILoginPresenter> {

	interface ILoginPresenter {
		public void login(String email, String password);
	};

}
