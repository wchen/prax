package com.praxiteles.module.main.presenter;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.user.client.ui.Widget;
import com.mvp4g.client.annotation.Presenter;
import com.mvp4g.client.presenter.LazyPresenter;
import com.praxiteles.library.Constant;
import com.praxiteles.module.main.MainEventBus;
import com.praxiteles.module.main.view.MyMoondogView;

@Presenter(view = MyMoondogView.class)
public class MyMoondogPresenter extends
		LazyPresenter<MyMoondogView, MainEventBus> implements
		MyMoondogView.IMyMoondogPresenter {

	public void goToSignup() {
		eventBus.signup();
	}

	public void goToLogin() {
		eventBus.login();
	}

	public void onSetMyMoondogForm(Widget form) {
		view.setForm(form);
	}

	public void onSetMyMoondogErrorMsg(String errorMsg) {
		view.setErrorMsgBorder(errorMsg.length() > 0);
		view.setErrorMsg(errorMsg);
	}

	public void onSetMyMoondogErrorMsgsMap(
			HashMap<String, ArrayList<String>> errorMsgsMap) {
		ArrayList<String> errorMsgs;
		for (String key : errorMsgsMap.keySet()) {
			errorMsgs = errorMsgsMap.get(key);
			for (String errorMsg : errorMsgs) {
				eventBus.setMyMoondogErrorMsg(errorMsg);
				break;
			}
			break;
		}
	}

	public void onMyMoondog(String param) {
		eventBus.setLandingPageBody(view);
		view.toggleLoginImage(false);
		view.toggleSignupImage(false);
		view.setErrorMsg("");
		view.setErrorMsgBorder(false);

		if (param.equals(Constant.LOGIN)) {
			view.toggleLoginImage(true);
		} else {
			view.toggleSignupImage(true);
		}
	}
}