package com.praxiteles.module.main.presenter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.Widget;
import com.mvp4g.client.annotation.Presenter;
import com.mvp4g.client.presenter.LazyPresenter;
import com.praxiteles.module.main.MainEventBus;
import com.praxiteles.module.main.presenter.interfaces.IMainView;
import com.praxiteles.module.main.view.MainView;

@Presenter(view = MainView.class)
public class MainPresenter extends LazyPresenter<IMainView, MainEventBus> {

	public void onNotFound() {
		GWT.log("History Not Found");
	}

	public void onSetView(Widget widget) {
		if (widget instanceof ResizeComposite)
			widget.setSize("100%", "100%");
		view.setWidget(widget);
		view.setStyleName("layoutView");
	}

	// public void onTestJson() {
	// boolean debugMode = true;
	// String userId = "cba0f713-4515-40c9-40a6-c4bc028a8d51";
	// final User userGet = new User(userId);
	// userGet.debugMode = debugMode;
	// userGet.read();
	//
	// final User userPost = new User();
	// userPost.debugMode = debugMode;
	// userPost.set(User.first_name, "first name");
	// GWT.log("first_name1: " + userPost.getLastErrorMessages());
	// userPost.set(
	// User.first_name,
	// "@invalid@invalid@invalid@invalid@invalid@invalid@invalid@invalid@invalid@invalid@invalid@invalid@invalid@invalid@invalid@invalid@invalid@invalid@invalid@invalid@invalid@invalid");
	// GWT.log("first_name2: " + userPost.getLastErrorMessages());
	// userPost.logErrorMsgs();
	//
	// userPost.set(User.first_name, "first name");
	// userPost.set(User.last_name, "last name");
	// userPost.set(User.email, "email");
	// userPost.set(User.password, "password");
	// userPost.set(User.company_name, "company name");
	// userPost.save();
	//
	// final User userPut = new User(userId);
	// userPut.debugMode = debugMode;
	// userPut.set(User.first_name, "first name2");
	// userPut.set(User.last_name, "last name2");
	// userPut.set(User.email, "email2");
	// userPut.set(User.password, "password2");
	// userPut.set(User.company_name, "company name2");
	// userPut.save();
	//
	// final User userDelete = new User(userId);
	// userDelete.debugMode = debugMode;
	// userDelete.delete();
	// }

	// public void onTestAws() {
	//
	// String policy = (new BASE64Encoder()).encode(
	// policy_document.getBytes("UTF-8")).replaceAll("\n","").replaceAll("\r","");
	//
	// Mac hmac = Mac.getInstance("HmacSHA1");
	// hmac.init(new SecretKeySpec(
	// aws_secret_key.getBytes("UTF-8"), "HmacSHA1"));
	// String signature = (new BASE64Encoder()).encode(
	// hmac.doFinal(policy.getBytes("UTF-8")))
	// .replaceAll("\n", "");
	// }
}
