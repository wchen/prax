package com.praxiteles.module.main.presenter;

import com.mvp4g.client.annotation.EventHandler;
import com.mvp4g.client.event.BaseEventHandler;
import com.praxiteles.library.Constant;
import com.praxiteles.module.main.MainEventBus;

@EventHandler
public class LandingPageEventHandler extends BaseEventHandler<MainEventBus> {
	public void onInitLandingPage(String param) {
		if (param.equals(Constant.LOGIN) || param.equals(Constant.SIGNUP)) {
			eventBus.myMoondog(param);
			if (param.equals(Constant.LOGIN))
				eventBus.initLogin();
			else
				eventBus.initSignup();
		} else if (param.equals(Constant.SLIDER)) {
			eventBus.initSlider();
		}
	}
}
