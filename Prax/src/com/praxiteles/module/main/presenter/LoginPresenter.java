package com.praxiteles.module.main.presenter;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.mvp4g.client.annotation.Presenter;
import com.mvp4g.client.presenter.LazyPresenter;
import com.praxiteles.library.Constant;
import com.praxiteles.library.Registry;
import com.praxiteles.library.model.User;
import com.praxiteles.library.service.AuthenticateService;
import com.praxiteles.module.main.MainEventBus;
import com.praxiteles.module.main.view.LoginView;

@Presenter(view = LoginView.class)
public class LoginPresenter extends LazyPresenter<LoginView, MainEventBus>
		implements LoginView.ILoginPresenter {

	private HashMap<String, ArrayList<String>> getValidationErrorMsg(
			String email, String password) {
		HashMap<String, ArrayList<String>> errorMsgsMap = new HashMap<String, ArrayList<String>>();

		if (!User.email.validate(email)) {
			errorMsgsMap.put(User.email.toString(),
					User.email.getErrorMessages());
		} else if (!User.password.validate(password)) {
			errorMsgsMap.put(User.password.toString(),
					User.password.getErrorMessages());
		}
		return errorMsgsMap;

	}

	public void login(String email, String password) {
		eventBus.setMyMoondogErrorMsg("");

		HashMap<String, ArrayList<String>> errorMsg = getValidationErrorMsg(
				email, password);
		if (errorMsg.size() > 0) {
			eventBus.setMyMoondogErrorMsgsMap(errorMsg);
			return;
		}
		AuthenticateService auth = AuthenticateService.instance();
		auth.authenticate(email, password, new RequestCallback() {

			public void onResponseReceived(Request request, Response response) {
				switch (response.getStatusCode()) {
				case 200:
					GWT.log("Login success, move on to next page.");
					eventBus.dashboard();
					break;
				case 400:
					eventBus.setMyMoondogErrorMsg((String) Registry
							.get(Registry.serviceErrorMsg));
					break;
				default:
					break;
				}
			}

			public void onError(Request request, Throwable exception) {
				GWT.log("Login failed, display error message.");
			}
		});
	}

	public void goToSignup() {
		eventBus.signup();
	}

	public void goToLogin() {
		eventBus.login();
	}

	public void onLogin() {
		eventBus.initLandingPage(Constant.LOGIN);
	}

	public void onInitLogin() {
		eventBus.setMyMoondogForm(view);
	}
}
