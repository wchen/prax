package com.praxiteles.module.main.presenter;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.mvp4g.client.annotation.Presenter;
import com.mvp4g.client.presenter.LazyPresenter;
import com.praxiteles.library.Constant;
import com.praxiteles.library.Registry;
import com.praxiteles.library.model.User;
import com.praxiteles.library.service.RecaptchaService;
import com.praxiteles.module.main.MainEventBus;
import com.praxiteles.module.main.view.SignupView;

@Presenter(view = SignupView.class)
public class SignupPresenter extends LazyPresenter<SignupView, MainEventBus>
		implements SignupView.ISignupPresenter {

	public void onSignup() {
		eventBus.initLandingPage(Constant.SIGNUP);
	}

	public void onInitSignup() {
		eventBus.setMyMoondogForm(view);
	}

	private HashMap<String, ArrayList<String>> getValidationErrorMsg(
			String firstName, String lastName, String email, String altEmail,
			String password, String confirmPassword, boolean terms) {

		HashMap<String, ArrayList<String>> errorMsgsMap = new HashMap<String, ArrayList<String>>();

		if (!terms) {
			ArrayList<String> msgs = new ArrayList<String>();
			msgs.add("You did not agree to the Terms of Service and Privacy Policy.");
			errorMsgsMap.put("terms", msgs);
		} else if (!password.equals(confirmPassword)) {
			ArrayList<String> msgs = new ArrayList<String>();
			msgs.add("The two entered passwords does not match.");
			errorMsgsMap.put("password", msgs);
		} else {
			User newUser = new User();
			newUser.set(User.firstName, firstName);
			newUser.set(User.lastName, lastName);
			newUser.set(User.email, email);
			if (altEmail.length() > 0) {
				newUser.set(User.altEmail, altEmail);
			}
			newUser.set(User.password, password);
			newUser.set(User.companyName, "Dummy Company Name");
			Registry.set(Registry.user, newUser);
			errorMsgsMap = newUser.getErrorMessages();
		}

		return errorMsgsMap;
	}

	private void resetRecaptcha() {
		view.resetRecaptchaChallenge();
		SignupView.reloadRecaptcha();
	}
	
	private void saveNewUserInfo() {
		User newUser = (User) Registry.get(Registry.user);
		newUser.setCustomCallback(new RequestCallback() {

			public void onResponseReceived(Request request, Response response) {
				switch (response.getStatusCode()) {
				case 200:
					GWT.log("Signup success, move on to next page.");
					break;
				case 400:
					eventBus.setMyMoondogErrorMsg((String) Registry
							.get(Registry.serviceErrorMsg));
					resetRecaptcha();
					break;
				}
			}

			public void onError(Request request, Throwable exception) {
				GWT.log("Error!");
			}
		});
		newUser.save();
	}

	private void sendRecaptchaAndUserInfo(String challenge, String response) {
		RecaptchaService recaptchaService = RecaptchaService.instance();
		recaptchaService.sendRecaptcha(challenge, response,
				new RequestCallback() {

					public void onResponseReceived(Request request,
							Response response) {
						switch (response.getStatusCode()) {
						case 200:
							saveNewUserInfo();
							break;
						case 400:
							eventBus.setMyMoondogErrorMsg((String) Registry
									.get(Registry.serviceErrorMsg));
							resetRecaptcha();
							break;
						}
					}

					public void onError(Request request, Throwable exception) {
						GWT.log("Error!");
					}
				});
	}

	public void signup(String recaptchaChallenge, String recaptchaResponse,
			String firstName, String lastName, String email, String altEmail,
			String password, String confirmPassword, boolean terms) {
		HashMap<String, ArrayList<String>> errorMsgsMap = getValidationErrorMsg(
				firstName, lastName, email, altEmail, password,
				confirmPassword, terms);
		eventBus.setMyMoondogErrorMsg("");
		if (errorMsgsMap.size() == 0) {
			sendRecaptchaAndUserInfo(recaptchaChallenge, recaptchaResponse);
		} else {
			eventBus.setMyMoondogErrorMsgsMap(errorMsgsMap);
			resetRecaptcha();
		}
	}
}
