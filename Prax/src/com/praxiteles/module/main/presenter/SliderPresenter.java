package com.praxiteles.module.main.presenter;

import java.util.ArrayList;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.DeckPanel;
import com.google.gwt.user.client.ui.Image;
import com.mvp4g.client.annotation.Presenter;
import com.mvp4g.client.presenter.LazyPresenter;
import com.praxiteles.library.Constant;
import com.praxiteles.module.main.MainEventBus;
import com.praxiteles.module.main.view.SliderView;

@Presenter(view = SliderView.class)
public class SliderPresenter extends LazyPresenter<SliderView, MainEventBus>
		implements SliderView.ISliderPresenter {
	private Timer timer;

	public void onHomepage() {
		eventBus.slider();
	}

	public void onSlider() {
		eventBus.initLandingPage(Constant.SLIDER);
	}

	public void onInitSlider() {
		eventBus.setLandingPageBody(view);
		view.getDeckPanel().clear();
		addImgPathsIntoDeckPanel(getImgPaths());
		loadImageBullets(view.getDeckPanel().getWidgetCount());
		shuffleSlidingImage();
	}

	private void loadImageBullets(int size) {
		view.clearBullets();
		for (int i = 0; i < size; ++i)
			view.addBulletImage(new Image(SliderView.GrayCircle));
	}

	public ArrayList<String> getImgPaths() {
		ArrayList<String> imgPaths = new ArrayList<String>();
		imgPaths.add("prax/image/moondog-image.png");
		return imgPaths;
	}

	public void addImgPathsIntoDeckPanel(ArrayList<String> imgPaths) {
		for (String imgPath : imgPaths)
			view.addSlidingImagePath(imgPath);
	}

	private void shuffleSlidingImage() {
		final DeckPanel deckPanel = view.getDeckPanel();
		view.showSlidingImage(0);
		if (timer == null) {
			timer = new Timer() {
				@Override
				public void run() {
					int index = deckPanel.getVisibleWidget();
					index++;
					if (index == deckPanel.getWidgetCount())
						index = 0;
					view.showSlidingImage(index);
				}
			};
		} else {
			timer.cancel();
		}
		timer.scheduleRepeating(5000);
	}
}