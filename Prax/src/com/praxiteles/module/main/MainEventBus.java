package com.praxiteles.module.main;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.user.client.ui.Widget;
import com.mvp4g.client.annotation.Debug;
import com.mvp4g.client.annotation.Debug.LogLevel;
import com.mvp4g.client.annotation.Event;
import com.mvp4g.client.annotation.Events;
import com.mvp4g.client.annotation.InitHistory;
import com.mvp4g.client.annotation.NotFoundHistory;
import com.mvp4g.client.annotation.module.ChildModule;
import com.mvp4g.client.annotation.module.ChildModules;
import com.mvp4g.client.annotation.module.DisplayChildModuleView;
import com.mvp4g.client.event.EventBus;
import com.praxiteles.historyConverter.InitHistoryConverter;
import com.praxiteles.historyConverter.LoginHistoryConverter;
import com.praxiteles.historyConverter.NotFoundHistoryConverter;
import com.praxiteles.historyConverter.SignupHistoryConverter;
import com.praxiteles.module.dashboard.DashboardModule;
import com.praxiteles.module.main.presenter.HeaderPresenter;
import com.praxiteles.module.main.presenter.LandingPageEventHandler;
import com.praxiteles.module.main.presenter.LandingPagePresenter;
import com.praxiteles.module.main.presenter.LoginPresenter;
import com.praxiteles.module.main.presenter.MainPresenter;
import com.praxiteles.module.main.presenter.MyMoondogPresenter;
import com.praxiteles.module.main.presenter.SignupPresenter;
import com.praxiteles.module.main.presenter.SliderPresenter;
import com.praxiteles.module.main.view.MainView;
import com.praxiteles.module.widgetWindow.WidgetWindowModule;

@ChildModules({
		@ChildModule(moduleClass = WidgetWindowModule.class, autoDisplay = false),
		@ChildModule(moduleClass = DashboardModule.class) })
@Debug(logLevel = LogLevel.SIMPLE)
@Events(startView = MainView.class, historyOnStart = true)
public interface MainEventBus extends EventBus {

	@InitHistory
	@Event(handlers = SliderPresenter.class, historyConverter = InitHistoryConverter.class)
	public void homepage();

	@NotFoundHistory
	@Event(handlers = MainPresenter.class, historyConverter = NotFoundHistoryConverter.class)
	public void notFound();

	@DisplayChildModuleView(DashboardModule.class)
	@Event(handlers = MainPresenter.class)
	public void setView(Widget widget);

	@Event(handlers = LoginPresenter.class, historyConverter = LoginHistoryConverter.class)
	public void login();

	@Event(handlers = LoginPresenter.class)
	public void initLogin();

	@Event(handlers = SignupPresenter.class, historyConverter = SignupHistoryConverter.class)
	public void signup();

	@Event(handlers = SignupPresenter.class)
	public void initSignup();

	@Event(handlers = SliderPresenter.class)
	public void slider();

	@Event(handlers = SliderPresenter.class)
	public void initSlider();

	@Event(handlers = { LandingPagePresenter.class, HeaderPresenter.class,
			LandingPageEventHandler.class })
	public void initLandingPage(String param);

	@Event(handlers = LandingPagePresenter.class)
	public void setLandingPageBody(Widget widget);

	@Event(handlers = LandingPagePresenter.class)
	public void setLandingPageHeader(Widget widget);

	@Event(handlers = MyMoondogPresenter.class)
	public void setMyMoondogForm(Widget form);

	@Event(handlers = MyMoondogPresenter.class)
	public void myMoondog(String param);

	@Event(handlers = MyMoondogPresenter.class)
	public void setMyMoondogErrorMsg(String errorMsg);

	@Event(handlers = MyMoondogPresenter.class)
	public void setMyMoondogErrorMsgsMap(
			HashMap<String, ArrayList<String>> errorMsgsMap);

	@Event(modulesToLoad = WidgetWindowModule.class)
	public void fileBrowser();

	@Event(modulesToLoad = DashboardModule.class)
	public void dashboard();

}
