package com.praxiteles.module.main.view;

import static com.google.gwt.query.client.GQuery.$;
import static gwtquery.plugins.draggable.client.Draggable.Draggable;
import gwtquery.plugins.draggable.client.DraggableOptions;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.DeckPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Singleton;
import com.praxiteles.library.widget.LazyReverseComposite;
import com.praxiteles.module.main.presenter.interfaces.ISliderView;
import com.praxiteles.module.main.presenter.interfaces.ISliderView.ISliderPresenter;

@Singleton
public class SliderView extends LazyReverseComposite<ISliderPresenter>
		implements ISliderView {
	private static SliderViewUiBinder uiBinder;

	interface SliderViewUiBinder extends UiBinder<Widget, SliderView> {
	}

	public void createView() {
		uiBinder = GWT.create(SliderViewUiBinder.class);
		bulletImages = new ArrayList<Image>();

		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiField
	DeckPanel deckPanel;
	@UiField
	ComplexPanel bullets;

	ArrayList<Image> bulletImages;
	public static String BlueCirle = "prax/image/blue-circle.png",
			GrayCircle = "prax/image/gray-circle.png";

	public void addSlidingImagePath(String imgSrc) {
		deckPanel.add(new Image(imgSrc));
	}

	public void showSlidingImage(int index) {
		int i = 0;
		for (Image bullet : bulletImages) {
			bullet.setUrl((i == index) ? SliderView.BlueCirle
					: SliderView.GrayCircle);
			++i;
		}

		deckPanel.showWidget(index);

	}

	public DeckPanel getDeckPanel() {
		return deckPanel;
	}

	public void addBulletImage(final Image img) {
		img.setStyleName("sliderBullets clickable draggable");
		img.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				int index = bulletImages.indexOf(img);
				showSlidingImage(index);
			}
		});
		bulletImages.add(img);
		bullets.add(img);
	}

	public void clearBullets() {
		bulletImages.clear();
		bullets.clear();
	}

	@Override
	public void onAttach() {
		super.onAttach();
		DraggableOptions o = new DraggableOptions();
		$(".draggable").as(Draggable).draggable(o);
	}
}
