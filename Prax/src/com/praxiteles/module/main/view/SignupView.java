package com.praxiteles.module.main.view;

import static com.google.gwt.query.client.GQuery.$;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Singleton;
import com.praxiteles.library.widget.LazyReverseComposite;
import com.praxiteles.module.main.presenter.interfaces.ISignupView;
import com.praxiteles.module.main.presenter.interfaces.ISignupView.ISignupPresenter;

@Singleton
public class SignupView extends LazyReverseComposite<ISignupPresenter>
		implements ISignupView {

	@UiField
	HasValue<String> firstName, lastName, email, altEmail, password,
			confirmPassword;
	@UiField
	HasValue<Boolean> terms;

	@Override
	protected void onAttach() {
		super.onAttach();
		bind();
	}

	@UiHandler("signup_button")
	void onSignupButtonClicked(ClickEvent e) {
		presenter.signup(getRecaptchaChallenge(), getRecaptchaResponse(),
				getFirstName(), getLastName(), getEmail(), getAltEmail(),
				getPassword(), getConfirmPassword(), getTerms());
	}

	public String getFirstName() {
		return firstName.getValue();
	}

	public String getLastName() {
		return lastName.getValue();
	}

	public String getEmail() {
		return email.getValue();
	}

	public String getAltEmail() {
		return altEmail.getValue();
	}

	public String getPassword() {
		return password.getValue();
	}

	public String getConfirmPassword() {
		return confirmPassword.getValue();
	}

	public boolean getTerms() {
		return terms.getValue();
	}

	private static SignupUiBinder uiBinder;

	interface SignupUiBinder extends UiBinder<Widget, SignupView> {
	}

	public void createView() {
		uiBinder = GWT.create(SignupUiBinder.class);
		initWidget(uiBinder.createAndBindUi(this));
	}

	public String getRecaptchaResponse() {
		return $("#recaptcha_response_field").val();
	};

	public String getRecaptchaChallenge() {
		return $("#recaptcha_challenge_field").val();
	};

	public void resetRecaptchaChallenge() {
		$("#recaptcha_challenge_field").val("");
	};

	public static native void reloadRecaptcha() /*-{
		$wnd.Recaptcha.reload();
	}-*/;

	public static native void bind() /*-{
		$wnd.Recaptcha.create("6LcB5cwSAAAAAEvENrSBAVi1gAJ5CvFoQFRifCN6", $doc
				.getElementById('recaptchaBox'), {
			theme : "clean",
			callback : $wnd.Recaptcha.focus_response_field
		});
	}-*/;

}
