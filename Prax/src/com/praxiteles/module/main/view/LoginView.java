package com.praxiteles.module.main.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Singleton;
import com.praxiteles.library.widget.LazyReverseComposite;
import com.praxiteles.module.main.presenter.interfaces.ILoginView;
import com.praxiteles.module.main.presenter.interfaces.ILoginView.ILoginPresenter;

@Singleton
public class LoginView extends LazyReverseComposite<ILoginPresenter> implements
		ILoginView {
	private static LoginUiBinder uiBinder;

	@UiField
	HasValue<String> email, password;

	@UiField
	SimplePanel loginForm;

	@UiHandler("login_button")
	void onLoginButtonClicked(ClickEvent e) {

		presenter.login(getEmail(), getPassword());
	}

	public String getEmail() {
		return email.getValue();
	}

	public String getPassword() {
		return password.getValue();
	}

	interface LoginUiBinder extends UiBinder<Widget, LoginView> {
	}

	public void createView() {
		uiBinder = GWT.create(LoginUiBinder.class);
		initWidget(uiBinder.createAndBindUi(this));
	}
}
