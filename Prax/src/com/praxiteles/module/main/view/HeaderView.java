package com.praxiteles.module.main.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Singleton;
import com.praxiteles.library.Util;
import com.praxiteles.library.widget.LazyReverseComposite;
import com.praxiteles.module.main.presenter.interfaces.IHeaderView;
import com.praxiteles.module.main.presenter.interfaces.IHeaderView.IHeaderPresenter;
import com.praxiteles.resource.LandingPageResource;

@Singleton
public class HeaderView extends LazyReverseComposite<IHeaderPresenter>
		implements IHeaderView {

	private static HeaderViewUiBinder uiBinder;

	interface HeaderViewUiBinder extends UiBinder<Widget, HeaderView> {
	}

	@UiField
	Image home, blog, myMoondog;

	@UiHandler("myMoondog")
	void onClickMyMoondog(ClickEvent e) {
		presenter.goToLogin();
	}

	@UiHandler({ "home", "logo" })
	void onClickHome(ClickEvent e) {
		presenter.goToSlider();
	}

	public void createView() {
		uiBinder = GWT.create(HeaderViewUiBinder.class);
		initWidget(uiBinder.createAndBindUi(this));
	}

	public void toggleMyMoondogImage(boolean toggle) {
		Util.toggleImageResource(myMoondog,
				LandingPageResource.INSTANCE.myMoondogBlue(),
				LandingPageResource.INSTANCE.myMoondogBlack(), toggle);
	}

	public void toggleHomeImage(boolean toggle) {
		Util.toggleImageResource(home, LandingPageResource.INSTANCE.homeBlue(),
				LandingPageResource.INSTANCE.homeBlack(), toggle);
	}

	public void toggleBlogImage(boolean toggle) {
		Util.toggleImageResource(blog, LandingPageResource.INSTANCE.blogBlue(),
				LandingPageResource.INSTANCE.blogBlack(), toggle);
	}

}
