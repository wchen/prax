package com.praxiteles.module.main.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Singleton;
import com.praxiteles.library.Util;
import com.praxiteles.library.widget.LazyReverseComposite;
import com.praxiteles.module.main.presenter.interfaces.IMyMoondogView;
import com.praxiteles.module.main.presenter.interfaces.IMyMoondogView.IMyMoondogPresenter;

@Singleton
public class MyMoondogView extends LazyReverseComposite<IMyMoondogPresenter>
		implements IMyMoondogView {

	@UiField
	Image signup, login;
	@UiField
	ComplexPanel centerBox, errorMsgBox;
	@UiField
	SimplePanel form;
	@UiField
	HasText errorMsg;

	@UiHandler("login")
	void onLoginTabClicked(ClickEvent e) {
		presenter.goToLogin();
	}

	@UiHandler("signup")
	void onSignupTabClicked(ClickEvent e) {
		presenter.goToSignup();
	}

	public void setForm(Widget form) {
		this.form.setWidget(form);
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg.setText(errorMsg);
	}

	public void setErrorMsgBorder(boolean activate) {
		errorMsgBox.setStyleName("errorMsgBox"
				+ ((activate) ? " errorMsgOn" : ""));
	}

	public void toggleLoginImage(boolean toggle) {
		Util.toggleImageUrl(login, "prax/image/login/login-static.png",
				"prax/image/login/login-picked.png", toggle);
	}

	public void toggleSignupImage(boolean toggle) {
		Util.toggleImageUrl(signup, "prax/image/signup/signup-static.png",
				"prax/image/signup/signup-picked.png", toggle);
	}

	private static MyMoondogViewUiBinder uiBinder;

	interface MyMoondogViewUiBinder extends UiBinder<Widget, MyMoondogView> {
	}

	public void createView() {
		uiBinder = GWT.create(MyMoondogViewUiBinder.class);
		initWidget(uiBinder.createAndBindUi(this));
	}

}
