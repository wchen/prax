package com.praxiteles.module.main.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Singleton;
import com.praxiteles.library.widget.LazyReverseResizeComposite;
import com.praxiteles.module.main.presenter.interfaces.ILandingPageView;
import com.praxiteles.module.main.presenter.interfaces.ILandingPageView.ILandingPagePresenter;

@Singleton
public class LandingPageView extends
		LazyReverseResizeComposite<ILandingPagePresenter> implements
		ILandingPageView {
	private static InstanceWidgetContainerViewUiBinder uiBinder;

	@UiField
	ComplexPanel centerPanel;
	@UiField(provided = true)
	Widget footer;
	@UiField
	SimplePanel header;

	interface InstanceWidgetContainerViewUiBinder extends
			UiBinder<Widget, LandingPageView> {
	}

	public void createView() {
		uiBinder = GWT.create(InstanceWidgetContainerViewUiBinder.class);
		footer = GWT.create(FooterView.class);
		initWidget(uiBinder.createAndBindUi(this));
	}

	public void setBody(Widget widget) {
		centerPanel.clear();
		if (widget instanceof ResizeComposite) {
			widget.setSize("100%", "100%");
		}
		centerPanel.add(widget);
	}

	public void setHeader(Widget widget) {
		header.setWidget(widget);
	}
}
