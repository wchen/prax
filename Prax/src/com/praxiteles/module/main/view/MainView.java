package com.praxiteles.module.main.view;

import com.google.gwt.user.client.ui.LazyPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Singleton;
import com.praxiteles.module.main.presenter.interfaces.IMainView;

@Singleton
public class MainView extends LazyPanel implements IMainView {

	public void createView() {
	}

	@Override
	protected Widget createWidget() {
		return new Widget();
	}

}
