package com.praxiteles.library;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.mvp4g.client.event.EventBusWithLookup;

public class EventBusClickHandler implements ClickHandler {
	private String eventName;
	private EventBusWithLookup eventBus;

	public EventBusClickHandler(EventBusWithLookup eventBus, String eventName) {
		this.eventName = eventName;
	}

	public void onClick(ClickEvent event) {
		eventBus.dispatch(eventName);
	}

}