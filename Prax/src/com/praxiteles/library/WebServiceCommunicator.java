package com.praxiteles.library;

import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.URL;

/**
 * WebServiceCommunicator class provides a communication interface with RESTful
 * webservice endpoint. The four RESTful methods are listed below:<br />
 * <ul>
 * <li>GET - Read: read()</li>
 * <li>POST - Create: save()</li>
 * <li>PUT - Update: update()</li>
 * <li>DELETE - Delete: delete()</li>
 * </ul>
 * 
 * @author Wensheng Chen
 * 
 */
public class WebServiceCommunicator {
	private String endpoint;
	private String requestData;
	private RequestCallback callback;
	private HashMap<String, String> headersMap;
	private HashMap<String, String> requestParamsMap;

	public WebServiceCommunicator(String endpoint) {
		this.endpoint = endpoint;
	}

	/**
	 * Converts hashmap to a URL encoded String in key=value format each pair is
	 * glued with a & character.
	 * 
	 * @param requestParams
	 *            HashMap &lt;String, String&gt;
	 * @return String
	 */
	private String requestParamsToString(HashMap<String, String> requestParams) {
		String toString = "";

		if (requestParams instanceof HashMap) {
			for (String key : requestParams.keySet()) {
				toString += URL.encode(key) + "="
						+ URL.encode(requestParams.get(key)) + "&";
			}
		}

		// Gobbles up the last ampersand character.
		if (toString.length() > 1) {
			toString = toString.substring(0, toString.length() - 1);
		}
		return toString;
	}

	/**
	 * Build and send a request to remote server.<br />
	 * This includes adding request headers, and payloads. <br />
	 * As well as attaching user defined callback.
	 * 
	 * @param method
	 *            RequestBuilder.Method
	 */
	private void sendRequest(RequestBuilder.Method method) {
		GWT.log("Crafting a request packet to remote server");

		setRequestParameters(method);
		ServiceRequest serviceRequest = new ServiceRequest(method, endpoint);
		setRequestHeaderAndBody(serviceRequest);

		// If user defined callback is defined, attach it.
		if (callback instanceof RequestCallback) {
			serviceRequest.setCallback(callback);
		}
		GWT.log("Sending " + method + " request to " + endpoint);
		serviceRequest.sendRequest();
	}

	/**
	 * Sets the headers and request data for the given RequestBuilder<br />
	 * Headers and request data are previously with
	 * 
	 * @param serviceRequest
	 *            RequestBuilder
	 */
	private void setRequestHeaderAndBody(RequestBuilder serviceRequest) {
		serviceRequest.setHeader("Content-Type",
				"application/x-www-form-urlencoded");

		if ((headersMap instanceof HashMap) && (headersMap.size() > 0)) {
			for (String key : headersMap.keySet()) {
				serviceRequest.setHeader(key, headersMap.get(key));
				GWT.log("Added header(\'" + key + "\', (\'"
						+ headersMap.get(key) + "\');");
			}
		}

		if ((requestData instanceof String) && (requestData.length() > 0)) {
			serviceRequest.setRequestData(requestData);
			GWT.log("Added request data " + requestData);
		}
	}

	/**
	 * If requestParams is set, append to url for GET action, append to form
	 * data for POST and PUT actions
	 * 
	 * @param method
	 *            RequestBuilder.Method
	 */
	private void setRequestParameters(RequestBuilder.Method method) {
		if ((requestParamsMap != null) && (requestParamsMap.size() > 0)) {
			if (method == RequestBuilder.GET) {
				endpoint += "?" + requestParamsToString(requestParamsMap);
			} else if ((method == RequestBuilder.POST)
					|| (method == RequestBuilder.PUT)) {
				requestData = requestParamsToString(requestParamsMap);
			}
		}
	}

	public void addRequestParameter(HashMap<String, String> requestParams) {
		requestParamsMap = requestParams;
	}

	/* Request Parameters */
	public void addRequestParameter(String key, String value) {
		if (requestParamsMap == null) {
			requestParamsMap = new HashMap<String, String>();
		}
		requestParamsMap.put(key, value);
	}

	public void removeRequestParameter(String key) {
		requestParamsMap.remove(key);
	}

	public void resetRequestParameters() {
		requestParamsMap = new HashMap<String, String>();
	}

	public void delete() {
		sendRequest(RequestBuilder.DELETE);
	}

	public String getHeader(String key) {
		return (headersMap != null) ? headersMap.get(key) : "";
	}

	/* Request Data */
	public String getRequestData() {
		return requestData;
	}

	public boolean hasRequestParameter(String key) {
		return (requestParamsMap != null) && requestParamsMap.containsKey(key);
	}

	/**
	 * This sends a GET request with no query string parameter
	 */
	public void get() {
		get(null);
	}

	/**
	 * This sends a GET request with query string parameters defined in the
	 * HashMap
	 * 
	 * @param requestParams
	 *            HashMap &lt;String, String&gt;
	 */
	public void get(HashMap<String, String> requestParams) {
		String requestParamsString = requestParamsToString(requestParams);
		if ((requestParamsString instanceof String)
				&& (requestParamsString.length() > 0)) {
			endpoint += "?" + requestParamsString;
		}

		sendRequest(RequestBuilder.GET);
	}

	/**
	 * This sends a POST request
	 */
	public void post() {
		post(null);
	}

	/**
	 * This sends a POST request with form data defined in the HashMap
	 * 
	 * @param requestParams
	 *            HashMap &lt;String, String&gt;
	 */
	public void post(HashMap<String, String> requestParams) {
		if ((requestParams instanceof HashMap) && (requestParams.size() > 0)) {
			requestData = requestParamsToString(requestParams);
		}
		sendRequest(RequestBuilder.POST);
	}

	/* Callback */
	public void setCallback(RequestCallback callback) {
		this.callback = callback;
	}

	/* Headers */
	public void setHeader(String key, String value) {
		if (headersMap == null) {
			headersMap = new HashMap<String, String>();
		}
		headersMap.put(key, value);
	}

	public void setRequestData(String requestData) {
		this.requestData = requestData;
	}

	/* Request Url */
	public void setEndpoint(String url) {
		endpoint = URL.encode(url);
	}

	/**
	 * This sends a PUT request
	 */
	public void put() {
		sendRequest(RequestBuilder.PUT);
	}

	/**
	 * This sends a PUT request with form data defined in the HashMap
	 * 
	 * @param requestParams
	 *            HashMap &lt;String, String&gt;
	 */
	public void put(HashMap<String, String> requestParams) {
		if (requestParams != null && requestParams.size() > 0) {
			requestData = requestParamsToString(requestParams);
		}
		sendRequest(RequestBuilder.PUT);
	}
}
