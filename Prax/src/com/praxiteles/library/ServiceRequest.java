package com.praxiteles.library;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.URL;

public class ServiceRequest extends RequestBuilder {

	public ServiceRequest(Method httpMethod, String url) {
		super(httpMethod, URL.encode(url));
	}

	public void sendRequest() {
		try {
			super.sendRequest(super.getRequestData(), super.getCallback());
		} catch (RequestException e) {
			GWT.log("Couldn't retrieve JSON");
		}
	}
}