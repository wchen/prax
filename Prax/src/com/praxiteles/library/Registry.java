package com.praxiteles.library;

import java.util.HashMap;

/**
 * Singleton Registry class that keeps track of variables globally. <br />
 * Useful for sharing variables with the rest of the project.
 * 
 * @author Wensheng Chen
 *
 */
public class Registry {
	public static String user = "User";
	public static String serviceErrorMsg = "ServiceErrorMsg";
	
	private static HashMap<String, Object> registryMap = new HashMap<String, Object>();
	private static Registry instance = new Registry();

	private Registry() {
	}

	public static Registry instance() {
		return instance;
	}

	/**
	 * Since the class hold a HashMap of Object, items retrieved from the registry <br />
	 * with the get method needs to be explicitly casted to their original type.
	 * 
	 * @param key String
	 * @return Object
	 */
	public static Object get(String key) {
		return registryMap.get(key);
	}

	public static Object set(String key, Object value) {
		return registryMap.put(key, value);
	}

}
