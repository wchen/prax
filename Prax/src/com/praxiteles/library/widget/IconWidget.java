package com.praxiteles.library.widget;

import gwtquery.plugins.draggable.client.DraggableOptions.RevertOption;
import gwtquery.plugins.draggable.client.gwt.DraggableWidget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.mvp4g.client.event.EventBusWithLookup;
import com.praxiteles.library.AppConfiguration;
import com.praxiteles.library.model.Icon;
import com.praxiteles.resource.IconWidgetResource;

public class IconWidget extends DraggableWidget<Composite> {

	private static IconWidgetUiBinder uiBinder = GWT
			.create(IconWidgetUiBinder.class);

	interface IconWidgetUiBinder extends UiBinder<Widget, IconWidget> {
	}

	public interface IconWidgetCss extends CssResource {
		String iconContainer();

		String iconImg();
	}

	private Icon instance;
	private String position;
	private EventBusWithLookup eventBus;
	
	@UiField
	Image img;

	@UiField
	Label label;
	
	@UiHandler("icon")
	void onIconClicked(ClickEvent e) {
		String eventName = instance.get(Icon.name);
		eventName = "open" + eventName.substring(0, 1).toUpperCase() + eventName.substring(1);
		eventBus.dispatch(eventName);
	}
	
	public IconWidget(Icon icon) {
		initWidget(uiBinder.createAndBindUi(this));
		init(icon);

		setDraggingCursor(Cursor.MOVE);
		setDraggingOpacity(0.8f);
		setContainment(".workspace");
		useCloneAsHelper();
		setRevert(RevertOption.ON_INVALID_DROP);
	}

	private void init(Icon icon) {
		instance = icon;
//		String imgUrl = AppConfiguration.ICON_IMAGE_BASEURL
//				+ instance.get(Icon.img);
		String imgUrl = "http://6.web.qstatic.com/webqqpic/pubapps/1/1656/images/big.png";
		img.setUrl(imgUrl);
		label.setText((String) instance.get(Icon.label));
		setPosition((Integer)instance.get(Icon.x), (Integer)instance.get(Icon.y));
	}

	@Override
	protected void onAttach() {
		super.onAttach();
		IconWidgetResource.INSTANCE.iconWidgetCss().ensureInjected();
	}

	public <T> void setPosition(int x, int y) {
		getElement().getStyle().setProperty("position", "absolute");
		getElement().getStyle().setPropertyPx("left",
				(Integer) x * AppConfiguration.ICON_X_WIDTH);
		getElement().getStyle().setPropertyPx("top",
				(Integer) y * AppConfiguration.ICON_Y_WIDTH);
		position = x + "," + y;
	}

	public void setEventBus(EventBusWithLookup eventBus) {
		this.eventBus = eventBus;
	}
	
	public String getPosition() {
		return position;
	}
	public Icon getIcon() {
		return instance;
	}

}
