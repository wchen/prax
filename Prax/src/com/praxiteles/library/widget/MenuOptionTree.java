package com.praxiteles.library.widget;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class MenuOptionTree extends Composite implements IMenuOption {

	private static MenuOptionTreeUiBinder uiBinder = GWT
			.create(MenuOptionTreeUiBinder.class);

	interface MenuOptionTreeUiBinder extends UiBinder<Widget, MenuOptionTree> {
	}

	private String optionTreeName;

	private ArrayList<MenuOption> menuOptions;

	public MenuOptionTree(String optionTreeName) {
		this.optionTreeName = optionTreeName;
		initWidget(uiBinder.createAndBindUi(this));
	}

	public MenuOptionTree(String optionTreeName,
			ArrayList<MenuOption> menuOptions) {
		this(optionTreeName);
		this.menuOptions = menuOptions;
	}

	@Override
	public Widget getWidget() {
		return this;
	}

	/** @TODO: expand the tree **/
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub

	}

}
