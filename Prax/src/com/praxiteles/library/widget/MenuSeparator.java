package com.praxiteles.library.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class MenuSeparator extends Composite implements IMenuOption {

	@Override
	public Widget getWidget() {
		return this;
	}

	private static MenuSeparatorUiBinder uiBinder = GWT
			.create(MenuSeparatorUiBinder.class);

	interface MenuSeparatorUiBinder extends UiBinder<Widget, MenuSeparator> {
	}

	public MenuSeparator() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	public void onClick(ClickEvent event) {
		// do nothing
	}

}
