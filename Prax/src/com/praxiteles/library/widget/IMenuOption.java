package com.praxiteles.library.widget;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;

public interface IMenuOption extends ClickHandler{
	public Widget getWidget();

}
