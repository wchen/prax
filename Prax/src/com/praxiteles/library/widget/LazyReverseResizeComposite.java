package com.praxiteles.library.widget;

import com.google.gwt.user.client.ui.ResizeComposite;
import com.mvp4g.client.view.LazyView;
import com.mvp4g.client.view.ReverseViewInterface;

public abstract class LazyReverseResizeComposite<T> extends ResizeComposite
		implements LazyView, ReverseViewInterface<T> {

	protected T presenter;

	public void setPresenter(T presenter) {
		this.presenter = presenter;
	}

	public T getPresenter() {
		return presenter;
	}

}
