package com.praxiteles.library.widget;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class Menu extends Composite {

	private static MenuUiBinder uiBinder = GWT.create(MenuUiBinder.class);

	private ArrayList<IMenuOption> menuOptions = new ArrayList<IMenuOption>();

	interface MenuUiBinder extends UiBinder<Widget, Menu> {
	}

	public Menu() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	public ArrayList<IMenuOption> getMenuOptions() {
		return menuOptions;
	}

	public void setMenuOptions(ArrayList<IMenuOption> menuOptions) {
		this.menuOptions = menuOptions;
	}

	public void addMenuOption(IMenuOption menuOption) {
		menuOptions.add(menuOption);
	}

}
