package com.praxiteles.library.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class MenuOption extends Composite implements IMenuOption {
	private String optionName;
	private ClickHandler handler;
	private static MenuOptionUiBinder uiBinder = GWT
			.create(MenuOptionUiBinder.class);

	interface MenuOptionUiBinder extends UiBinder<Widget, MenuOption> {
	}

	public MenuOption(String optionName, ClickHandler handler) {
		this.optionName = optionName;
		this.handler = handler;
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public Widget getWidget() {
		return this;
	}

	public void onClick(ClickEvent event) {
		handler.onClick(event);
	}

}
