package com.praxiteles.library.widget;

import com.google.gwt.user.client.ui.Composite;
import com.mvp4g.client.view.LazyView;
import com.mvp4g.client.view.ReverseViewInterface;

public abstract class LazyReverseComposite<T> extends Composite implements LazyView,
		ReverseViewInterface<T> {

	protected T presenter;

	public void setPresenter(T presenter) {
		this.presenter = presenter;
	}

	public T getPresenter() {
		return presenter;
	}

}
