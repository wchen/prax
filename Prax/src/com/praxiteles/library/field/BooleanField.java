package com.praxiteles.library.field;

public class BooleanField extends Field {
	private static Boolean type = new Boolean(false);

	public BooleanField(String key) {
		super(key);
	}

	@Override
	public Object getType() {
		return BooleanField.type;
	}

}
