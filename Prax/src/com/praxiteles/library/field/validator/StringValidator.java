package com.praxiteles.library.field.validator;

import com.google.gwt.regexp.shared.RegExp;
import com.praxiteles.library.field.validator.IValidator.IBiParametricValidator;

class StringValidator {
	static class LengthLessThan implements IBiParametricValidator {
		private static LengthLessThan instance;

		private LengthLessThan() {
		}

		static LengthLessThan instance() {
			if (instance == null)
				instance = new LengthLessThan();
			return instance;
		}

		public boolean validate(Object data, Object data2) {
			return ((String) data).length() < (Integer) data2;
		}

		public String getErrorMessage(Object data2) {
			return "String length has to be less than "
					+ ((Integer) data2).toString() + " characters.";
		}

	}

	static class LengthGreaterThan implements IBiParametricValidator {
		private static LengthGreaterThan instance;

		private LengthGreaterThan() {
		}

		static LengthGreaterThan instance() {
			if (instance == null)
				instance = new LengthGreaterThan();
			return instance;
		}

		public boolean validate(Object data, Object data2) {
			return ((String) data).length() > (Integer) data2;
		}

		public String getErrorMessage(Object data2) {
			return "String length has to be greater than "
					+ ((Integer) data2).toString() + " characters.";
		}

	}

	static class IsDigits implements IValidator {
		private static IsDigits instance;

		private IsDigits() {
		}

		static IsDigits instance() {
			if (instance == null)
				instance = new IsDigits();
			return instance;
		}

		public boolean validate(Object data) {
			RegExp regExp = RegExp.compile("^[0-9]*$");
			return regExp.test((String) data);
		}

		public String getErrorMessage() {
			return "String is not digits.";
		}

	}

	static class IsLetters implements IValidator {
		private static IsLetters instance;

		private IsLetters() {
		}

		static IsLetters instance() {
			if (instance == null)
				instance = new IsLetters();
			return instance;
		}

		public boolean validate(Object data) {
			RegExp regExp = RegExp.compile("^[A-Za-z]*$");
			return regExp.test((String) data);
		}

		public String getErrorMessage() {
			return "String is not letters.";
		}

	}

	static class IsAlphaNumeric implements IValidator {
		private static IsAlphaNumeric instance;

		private IsAlphaNumeric() {
		}

		static IsAlphaNumeric instance() {
			if (instance == null)
				instance = new IsAlphaNumeric();
			return instance;
		}

		public boolean validate(Object data) {
			RegExp regExp = RegExp.compile("^[0-9A-Za-z]*$");
			return regExp.test((String) data);
		}

		public String getErrorMessage() {
			return "String is not alphanumeric.";
		}

	}
	
	static class IsAlphaNumericOrSpace implements IValidator {
		private static IsAlphaNumericOrSpace instance;

		private IsAlphaNumericOrSpace() {
		}

		static IsAlphaNumericOrSpace instance() {
			if (instance == null)
				instance = new IsAlphaNumericOrSpace();
			return instance;
		}

		public boolean validate(Object data) {
			RegExp regExp = RegExp.compile("^[0-9A-Za-z ]*$");
			return regExp.test((String) data);
		}

		public String getErrorMessage() {
			return "String is not alphanumeric or space.";
		}

	}

	static class IsEmail implements IValidator {
		private static IsEmail instance;

		private IsEmail() {
		}

		static IsEmail instance() {
			if (instance == null)
				instance = new IsEmail();
			return instance;
		}

		public boolean validate(Object data) {
			RegExp regExp = RegExp
					.compile("^.+@.+\\.[a-zA-Z]{2,4}$");
			return regExp.test((String) data);
		}

		public String getErrorMessage() {
			return "String is not in email format.";
		}
	}

}
