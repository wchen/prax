package com.praxiteles.library.field.validator;

public interface IValidator {
	public interface IBiParametricValidator {
		public boolean validate(Object data, Object data2);
		public String getErrorMessage(Object data2);
	}
	
	public boolean validate(Object data);
	public String getErrorMessage();
}
