package com.praxiteles.library.field.validator;

public class ValidatorFactory {
	private ValidatorFactory() {
	}

	public static StringValidator.IsEmail getStringIsEmail() {
		return StringValidator.IsEmail.instance();
	}

	public static StringValidator.IsDigits getStringIsDigits() {
		return StringValidator.IsDigits.instance();
	}

	public static StringValidator.IsLetters getStringIsLetters() {
		return StringValidator.IsLetters.instance();
	}

	public static StringValidator.IsAlphaNumeric getStringIsAlphaNumeric() {
		return StringValidator.IsAlphaNumeric.instance();
	}

	public static StringValidator.IsAlphaNumericOrSpace getStringIsAlphaNumericOrSpace() {
		return StringValidator.IsAlphaNumericOrSpace.instance();
	}

	public static StringValidator.LengthGreaterThan getStringLengthGreaterThan() {
		return StringValidator.LengthGreaterThan.instance();
	}

	public static StringValidator.LengthLessThan getStringLengthLessThan() {
		return StringValidator.LengthLessThan.instance();
	}

	public static NumberValidator.LessThan getNumberLessThan() {
		return NumberValidator.LessThan.instance();
	}

	public static NumberValidator.GreaterThan getNumberGreaterThan() {
		return NumberValidator.GreaterThan.instance();
	}

	public static NumberValidator.EqualTo getNumberEqualTo() {
		return NumberValidator.EqualTo.instance();
	}

	public static NumberValidator.HasPrecisionLessThan getNumberHasPrecisionLessThan() {
		return NumberValidator.HasPrecisionLessThan.instance();
	}

	public static NumberValidator.IsUnsigned getNumberIsUnsigned() {
		return NumberValidator.IsUnsigned.instance();
	}

}
