package com.praxiteles.library.field.validator;

import com.google.gwt.regexp.shared.RegExp;
import com.praxiteles.library.field.validator.IValidator.IBiParametricValidator;

class NumberValidator {
	static class LessThan implements IBiParametricValidator {
		private static LessThan instance;

		private LessThan() {
		}

		static LessThan instance() {
			if (instance == null)
				instance = new LessThan();
			return instance;
		}

		public boolean validate(Object data, Object data2) {
			return (Double) data < (Double) data2;
		}

		public String getErrorMessage(Object data2) {
			return "Number has to be less than " + (Double) data2 + ".";
		}
	}

	static class GreaterThan implements IBiParametricValidator {
		private static GreaterThan instance;

		private GreaterThan() {
		}

		static GreaterThan instance() {
			if (instance == null)
				instance = new GreaterThan();
			return instance;
		}

		public boolean validate(Object data, Object data2) {
			return (Double) data > (Double) data2;
		}

		public String getErrorMessage(Object data2) {
			return "Number has to be greater than " + (Double) data2 + ".";
		}
	}

	static class EqualTo implements IBiParametricValidator {
		private static EqualTo instance;

		private EqualTo() {
		}

		static EqualTo instance() {
			if (instance == null)
				instance = new EqualTo();
			return instance;
		}

		public boolean validate(Object data, Object data2) {
			return (Double) data == (Double) data2;
		}

		public String getErrorMessage(Object data2) {
			return "Number can only be " + (Double) data2 + ".";
		}
	}

	static class HasPrecisionLessThan implements
			IBiParametricValidator {
		private static HasPrecisionLessThan instance;

		private HasPrecisionLessThan() {
		}

		static HasPrecisionLessThan instance() {
			if (instance == null)
				instance = new HasPrecisionLessThan();
			return instance;
		}

		public boolean validate(Object data, Object data2) {
			RegExp regExp = RegExp.compile("^[0-9]+\\.[0-9]{0, "
					+ (Integer) data2 + "}$");
			return regExp.test(((Double) data).toString());
		}

		public String getErrorMessage(Object data2) {
			return "Number cannot have more than " + (Integer) data2
					+ " digits after decimal point.";
		}
	}

	static class IsUnsigned implements IBiParametricValidator {
		private static IsUnsigned instance;

		private IsUnsigned() {
		}

		static IsUnsigned instance() {
			if (instance == null)
				instance = new IsUnsigned();
			return instance;
		}

		public boolean validate(Object data, Object data2) {
			return ((Boolean) data2) ? ((Double) data) >= 0 : true;
		}

		public String getErrorMessage(Object data2) {
			return "Number cannot be negative.";
		}

	}

}
