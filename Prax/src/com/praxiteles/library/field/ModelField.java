package com.praxiteles.library.field;

import com.praxiteles.library.model.ModelVirtualProxy;

public class ModelField<T extends ModelVirtualProxy> extends Field {
	private T type;

	public ModelField(String key, T type) {
		super(key);
		this.type = type;
	}

	@Override
	public Object getType() {
		return type;
	}
}
