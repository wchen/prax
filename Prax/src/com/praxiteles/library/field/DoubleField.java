package com.praxiteles.library.field;

public class DoubleField extends Field {
	private static Double type = new Double(0);

	public DoubleField(String key) {
		super(key);
	}

	@Override
	public Object getType() {
		return DoubleField.type;
	}

}
