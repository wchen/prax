package com.praxiteles.library.field;

import com.praxiteles.library.field.validator.ValidatorFactory;

public class EmailField extends StringField {
	public EmailField(String key) {
		super(key);
		addValidator(ValidatorFactory.getStringIsEmail());
	}
}
