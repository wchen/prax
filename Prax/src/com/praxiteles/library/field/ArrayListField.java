package com.praxiteles.library.field;

import java.util.ArrayList;
import java.util.List;

public class ArrayListField<T> extends Field {
	private T type;

	public ArrayListField(String key, T type) {
		super(key);
		this.type = type;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class<? extends List> getCollectionType() {
		return ArrayList.class;
	}

	@Override
	public Object getType() {
		return type;
	}
}
