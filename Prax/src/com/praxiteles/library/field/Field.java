package com.praxiteles.library.field;

import java.util.ArrayList;
import java.util.List;

import com.praxiteles.library.Util;
import com.praxiteles.library.field.validator.IValidator;

/**
 * Field class is used by all models, it provides information needed to get the
 * right data type from JSO. <br />
 * It also provides an interface to add validators to the field.
 * 
 * @author Wensheng Chen
 * 
 */
public abstract class Field implements IValidator {
	private class Validator {
		protected String errorMsg;
		protected Object validator;

		Validator() {
		}

		Validator(IValidator validator) {
			this.validator = validator;
			errorMsg = Util.convertUnderscoreToTitleCase(Field.this.toString())
					+ ": " + validator.getErrorMessage();
		}

		void setErrorMsg(String errorMsg) {
			this.errorMsg = errorMsg;
		}

		boolean validate(Object data) {
			return ((IValidator) validator).validate(data);
		}

		String getErrorMsg() {
			return errorMsg;
		}
	}

	private class BiParametricValidator extends Validator {
		Object constraint;

		BiParametricValidator(IBiParametricValidator validator,
				Object constraint) {
			this.validator = validator;
			this.constraint = constraint;
			errorMsg = Util.convertUnderscoreToTitleCase(Field.this.toString())
					+ ": " + validator.getErrorMessage(constraint);
		}

		@Override
		boolean validate(Object data) {
			return ((IBiParametricValidator) validator).validate(data,
					constraint);
		}
	}

	protected final String key;

	private ArrayList<Validator> validators = new ArrayList<Validator>();
	private ArrayList<String> fieldErrorMsgs = new ArrayList<String>();

	public Field(String key) {
		this.key = key;
	}

	public Field addValidator(IValidator aValidator, String errorMsg) {
		Validator newValidator = new Validator(aValidator);
		newValidator.setErrorMsg(errorMsg);
		validators.add(newValidator);
		return this;
	}

	public Field addValidator(IValidator aValidator) {
		Validator newValidator = new Validator(aValidator);
		validators.add(newValidator);
		return this;
	}

	public Field addValidator(IBiParametricValidator aValidator,
			Object constraint) {
		Validator newValidator = new BiParametricValidator(aValidator,
				constraint);
		validators.add(newValidator);
		return this;
	}

	public Field addValidator(IBiParametricValidator aValidator,
			Object constraint, String errorMsg) {
		Validator newValidator = new BiParametricValidator(aValidator,
				constraint);
		newValidator.setErrorMsg(errorMsg);
		validators.add(newValidator);
		return this;
	}

	/**
	 * Validates the input data against all the validators register to this
	 * field object. <br />
	 * Returns true if the input passes all validations without an error.
	 * 
	 * @param data
	 *            Object
	 * @return boolean
	 */
	public final boolean validate(Object data) {
		fieldErrorMsgs.clear();

		for (Validator aValidator : validators) {
			if (!aValidator.validate(data))
				fieldErrorMsgs.add(aValidator.getErrorMsg());
		}

		return fieldErrorMsgs.size() == 0;
	}

	public String getErrorMessage() {
		return fieldErrorMsgs.size() == 0 ? "" : fieldErrorMsgs.get(0);
	}

	public ArrayList<String> getErrorMessages() {
		return fieldErrorMsgs;
	}

	public abstract Object getType();

	@SuppressWarnings("rawtypes")
	public Class<? extends List> getCollectionType() {
		return null;
	}

	@Override
	public String toString() {
		return key;
	}
}
