package com.praxiteles.library.field;

public class StringField extends Field {
	private static String type = new String("");

	public StringField(String key) {
		super(key);
	}

	@Override
	public Object getType() {
		return StringField.type;
	}
}
