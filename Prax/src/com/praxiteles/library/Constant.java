package com.praxiteles.library;

/**
 * Application wide Constants are stored here.
 * 
 * @author Wensheng Chen
 *
 */
public class Constant {
	public static final String SLIDER 	= "slider";
	public static final String LOGIN 	= "login";
	public static final String SIGNUP 	= "signup";
	public static final String BLOG 	= "blog";
}
