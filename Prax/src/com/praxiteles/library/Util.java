package com.praxiteles.library;

import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Image;

/**
 * Util class provides a suite of commonly functions.
 * 
 * @author Wensheng Chen
 *
 */
public class Util {
	
	/**
	 * Example input: "this_is_a_test" will return "This Is A Test"
	 * 
	 * @param underscore String
	 * @return String
	 */
	public static String convertUnderscoreToTitleCase(String underscore) {
		// First character is upper case
		boolean hasUnderscore = true;
		StringBuilder titleCaseWord = new StringBuilder();

		for (int i = 0; i < underscore.length(); ++i) {
			if (underscore.charAt(i) == '_') {
				titleCaseWord.append(' ');
				hasUnderscore = true;
			} else if (hasUnderscore) {
				titleCaseWord.append(Character.toUpperCase(underscore
						.charAt(i)));
				hasUnderscore = false;
			} else {
				titleCaseWord.append(underscore.charAt(i));
			}
		}
		return titleCaseWord.toString();
	}

	public static void toggleImageUrl(Image image, String deactive,
			String active, boolean toggle) {
		if (!toggle && !image.getUrl().equals(deactive)) {
			image.setUrl(deactive);
		} else if (toggle && !image.getUrl().equals(active)) {
			image.setUrl(active);
		}
	}

	public static void toggleImageResource(Image image, ImageResource deactive,
			ImageResource active, boolean toggle) {
		image.setResource((toggle) ? active : deactive);
	}
}
