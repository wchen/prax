package com.praxiteles.library.model;

import java.util.HashMap;

import com.praxiteles.library.AppConfiguration;
import com.praxiteles.library.field.BooleanField;
import com.praxiteles.library.field.EmailField;
import com.praxiteles.library.field.Field;
import com.praxiteles.library.field.StringField;
import com.praxiteles.library.field.validator.ValidatorFactory;

public class User extends ModelProxy {
	public final static StringField firstName;
	public final static StringField lastName;
	public final static StringField password;
	public final static StringField id;
	public final static EmailField email;
	public final static EmailField altEmail;
	public final static StringField companyName;
	public final static StringField lastLogin;
	public final static BooleanField active;

	// Leave group right now.
	// @SuppressWarnings({ "rawtypes", "unchecked" })
	// public final static SimpleEntry groups = new SimpleEntry("groups",
	// new SimpleEntry(ArrayList.class, Group.class));
	private static HashMap<String, Field> fieldsMap;

	static {
		firstName = new StringField("first_name");
		firstName
				.addValidator(ValidatorFactory.getStringLengthGreaterThan(), 0)
				.addValidator(ValidatorFactory.getStringLengthLessThan(), 51)
				.addValidator(ValidatorFactory.getStringIsAlphaNumericOrSpace());

		lastName = new StringField("last_name");
		lastName
				.addValidator(ValidatorFactory.getStringLengthGreaterThan(), 0)
				.addValidator(ValidatorFactory.getStringLengthLessThan(), 51)
				.addValidator(ValidatorFactory.getStringIsAlphaNumericOrSpace());

		password = new StringField("password");
		password.addValidator(ValidatorFactory.getStringLengthGreaterThan(), 5)
				.addValidator(ValidatorFactory.getStringLengthLessThan(), 20);

		id = new StringField("id");

		email = new EmailField("email");
		email.addValidator(ValidatorFactory.getStringLengthLessThan(), 51);

		altEmail = new EmailField("alt_email");
		altEmail.addValidator(ValidatorFactory.getStringLengthLessThan(), 51);

		companyName = new StringField("company_name");
		companyName
				.addValidator(ValidatorFactory.getStringLengthGreaterThan(), 0)
				.addValidator(ValidatorFactory.getStringLengthLessThan(), 121)
				.addValidator(ValidatorFactory.getStringIsAlphaNumericOrSpace());

		lastLogin = new StringField("last_login");
		active = new BooleanField("active");

		fieldsMap = new HashMap<String, Field>();
		put(fieldsMap, firstName);
		put(fieldsMap, lastName);
		put(fieldsMap, password);
		put(fieldsMap, id);
		put(fieldsMap, email);
		put(fieldsMap, altEmail);
		put(fieldsMap, companyName);
		put(fieldsMap, lastLogin);
		put(fieldsMap, active);
	}

	@Override
	public HashMap<String, Field> getFields() {
		return User.fieldsMap;
	}

	public User() {
		this(null);
	}

	public User(String id) {
		super(id);
	}

	@Override
	public String getEndpoint() {
		return AppConfiguration.USER_ENDPOINT;
	}

	@Override
	public Field getIdentifierKey() {
		return User.id;
	}

	@Override
	protected ModelVirtualProxy getModel(
			Class<? extends ModelVirtualProxy> modelType, JSOModel jsoModel) {
		return modelType == Group.class ? new Group(jsoModel) : null;
	}

}