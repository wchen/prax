package com.praxiteles.library.model;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayString;
import com.praxiteles.library.field.Field;

/**
 * Maybe not totally a Virtual Proxy as I hide some function from the interface. <br />
 * This class keeps track of data in form of cache and lazy loads the data only
 * when needed.
 * 
 * @author wen
 * 
 */
public class ModelVirtualProxy {
	private HashMap<String, Object> cachedFieldsMap = new HashMap<String, Object>();
	private HashMap<String, ArrayList<String>> errorMsgsMap;
	protected ModelInterface data;
	private boolean isModified, bypassValidation;
	private String lastFieldFailed, jsoString = "";

	public ModelVirtualProxy() {
	}

	public ModelVirtualProxy(String jsoString) {
		this.jsoString = jsoString;
	}

	public void bypassValidation() {
		bypassValidation = true;
	}

	public void setData(ModelInterface data) {
		cachedFieldsMap.clear();
		this.data = data;
	}

	private void initJsoModel() {
		if (data == null) {
			data = (jsoString.equals("")) ? JSOModel.create() : JSOModel
					.fromJson(jsoString);
		}
	}

	public JsArrayString keys() {
		initJsoModel();

		JsArrayString keys = data.keys();
		for (String key : cachedFieldsMap.keySet()) {
			keys.push(key);
		}
		return keys;
	}

	public boolean isModified() {
		return isModified;
	}

	public void setModified(boolean modified) {
		isModified = modified;
	}

	public HashMap<String, ?> getCachedFields() {
		return cachedFieldsMap;
	}

	public void setJsonString(String jsoString) {
		this.jsoString = jsoString;
	}

	public void setCachedFields(HashMap<String, Object> fields) {
		cachedFieldsMap = fields;
	}

	public void clearCachedFields() {
		cachedFieldsMap.clear();
	}

	/* JSOModel fields start */
	protected ModelVirtualProxy getModel(
			Class<? extends ModelVirtualProxy> modelType, JSOModel jsoModel) {
		return null;
	}

	protected String getString(String key) {
		return data.get(key);
	}

	protected Boolean getBoolean(String key) {
		return data.getBoolean(key);
	}

	protected Integer getInt(String key) {
		return data.getInt(key);
	}

	protected Double getDouble(String key) {
		return data.getDouble(key);
	}

	protected JSOModel getObject(String key) {
		return data.getObject(key);
	}

	protected JsArray<JSOModel> getArray(String key) {
		return data.getArray(key);
	}

	protected <T> ArrayList<T> getArrayList(String key, T type) {
		return data.getArrayList(key, type);
	}

	/**
	 * Reads an array of fieldType models with polymorphic getModel method from
	 * key field.
	 * 
	 * @param key
	 *            String
	 * @param fieldType
	 *            Class
	 * @return ArrayList of ModelBase
	 */
	@SuppressWarnings("unchecked")
	protected <T extends ModelVirtualProxy> ArrayList<T> getArrayList(
			String key, T modelType) {
		JsArray<JSOModel> jsoModels = data.getArray(key);
		ArrayList<T> models = new ArrayList<T>();
		T model;

		for (int i = 0; i < jsoModels.length(); ++i) {
			model = (T) getModel(modelType.getClass(), jsoModels.get(i));
			models.add(model);
		}
		return models;
	}

	/**
	 * Gets appropriate value based on the type of field, key. <br/>
	 * Returns null if value is not found.
	 * 
	 * @param key
	 *            Field
	 * @return Object
	 */
	@SuppressWarnings("unchecked")
	public <T> T get(Field key) {
		String keyName = key.toString();
		if (cachedFieldsMap.containsKey(keyName)) {
			return (T) cachedFieldsMap.get(keyName);
		} else {
			initJsoModel();
		}

		T value = getValueFromField(key);

		if (value != null && "null".equals(value) || "undefined".equals(value)) {
			value = null;
		}

		cachedFieldsMap.put(keyName, value);
		return value;
	}

	@SuppressWarnings("unchecked")
	private <T> T getValueFromField(Field key) {
		String keyName = key.toString();
		T fieldType = (T) key.getType();
		T value = null;
		if (fieldType instanceof String) {
			value = (T) getString(keyName);
		} else if (fieldType instanceof Boolean) {
			value = (T) getBoolean(keyName);
		} else if (fieldType instanceof Integer) {
			value = (T) getInt(keyName);
		} else if (fieldType instanceof Double) {
			value = (T) getDouble(keyName);
		} else if (fieldType instanceof JSOModel) {
			value = (T) getObject(keyName);
		} else if (fieldType instanceof JsArray) {
			value = (T) getArray(keyName);
		} else if (fieldType instanceof ModelVirtualProxy) {
			value = (T) getModel(
					(Class<? extends ModelVirtualProxy>) fieldType.getClass(),
					getObject(keyName));
		}

		return value;
	}

	@SuppressWarnings("unchecked")
	public <T> ArrayList<T> getArray(Field key) {
		String keyName = key.toString();
		T fieldType = (T) key.getType();
		if (cachedFieldsMap.containsKey(keyName)) {
			return (ArrayList<T>) cachedFieldsMap.get(keyName);
		} else {
			initJsoModel();
		}

		ArrayList<T> value = null;
		if (fieldType instanceof ModelVirtualProxy) {
			value = (ArrayList<T>) getArrayList(keyName,
					(ModelVirtualProxy) fieldType);
		} else {
			value = getArrayList(keyName, fieldType);
		}

		if (value != null && "null".equals(value) || "undefined".equals(value)) {
			value = null;
		}

		cachedFieldsMap.put(keyName, value);
		return value;
	}

	public void set(Field key, boolean value) {
		set(key, (Boolean) value);
	}

	public void set(Field key, int value) {
		set(key, (Integer) value);
	}

	public void set(Field key, double value) {
		set(key, (Double) value);
	}

	public void set(Field key, Object value) {
		if (bypassValidation || key.validate(value)) {
			isModified = true;
			cachedFieldsMap.put(key.toString(), value);
		} else {
			putErrorMsg(key.toString(), key.getErrorMessages());
		}
	}

	public void setArray(Field key, ArrayList<? extends Object> values) {
		if (bypassValidation || key.validate(values)) {
			isModified = true;
			cachedFieldsMap.put(key.toString(), values);
		} else {
			putErrorMsg(key.toString(), key.getErrorMessages());
		}
	}

	private void putErrorMsg(String key, ArrayList<String> msgs) {
		lastFieldFailed = key;
		if (errorMsgsMap == null)
			errorMsgsMap = new HashMap<String, ArrayList<String>>();
		errorMsgsMap.put(key, msgs);
	}

	/* JSOModel fields end */

	/* Validation and Error Message start */
	public HashMap<String, ArrayList<String>> getErrorMessages() {
		return errorMsgsMap;
	}

	public ArrayList<String> getErrorMessagesFromField(Field key) {
		return errorMsgsMap.get(key.toString());
	}

	public String getLastErrorMessage() {
		if (lastFieldFailed == null)
			return "";

		ArrayList<String> errorMsgs = errorMsgsMap.get(lastFieldFailed);
		return errorMsgs.get(errorMsgs.size() - 1);
	}

	public void logErrorMsgs() {
		HashMap<String, ArrayList<String>> errorMsgsMap = getErrorMessages();
		ArrayList<String> errorMsgs;
		for (String key : errorMsgsMap.keySet()) {
			errorMsgs = errorMsgsMap.get(key);
			for (String errorMsg : errorMsgs)
				GWT.log(key + ": " + errorMsg);
		}
	}

	public void clearErrorMessages() {
		errorMsgsMap.clear();
	}
	/* Validation and Error Message end */
}
