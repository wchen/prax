package com.praxiteles.library.model;

import java.util.HashMap;

import com.praxiteles.library.AppConfiguration;
import com.praxiteles.library.field.ArrayListField;
import com.praxiteles.library.field.Field;
import com.praxiteles.library.field.IntegerField;
import com.praxiteles.library.field.ModelField;
import com.praxiteles.library.field.StringField;

public class Workspace extends ModelProxy {
	public final static IntegerField id;
	public final static StringField bgImg;
	public final static ModelField<Menu> rClickMenu;
	public final static ArrayListField<Icon> icons;

	private static HashMap<String, Field> fieldsMap;

	static {
		id = new IntegerField("id");
		bgImg = new StringField("bg_img");
		rClickMenu = new ModelField<Menu>("rclick_menu", new Menu());
		icons = new ArrayListField<Icon>("icons", new Icon());

		fieldsMap = new HashMap<String, Field>();
		put(fieldsMap, id);
		put(fieldsMap, bgImg);
		put(fieldsMap, rClickMenu);
		put(fieldsMap, icons);
	}

	public Workspace() {
		this((String) null);
	}

	public Workspace(ModelInterface data) {
		super(data);
	}

	public Workspace(String id) {
		super(id);
	}

	@Override
	public HashMap<String, Field> getFields() {
		return Workspace.fieldsMap;
	}

	@Override
	public Field getIdentifierKey() {
		return Workspace.id;
	}

	@Override
	public String getEndpoint() {
		return AppConfiguration.DASHBOARD_ENDPOINT;
	}

	@Override
	protected ModelVirtualProxy getModel(
			Class<? extends ModelVirtualProxy> modelType, JSOModel jsoModel) {
		if (modelType == Menu.class)
			return new Menu(jsoModel);
		else if (modelType == Icon.class)
			return new Icon(jsoModel);
		else
			return null;
	}

}
