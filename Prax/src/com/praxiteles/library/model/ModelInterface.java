package com.praxiteles.library.model;

import java.util.ArrayList;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayString;

public interface ModelInterface {
	public String get(String key);

	public void set(String key, Boolean value);

	public void set(String key, int value);

	public void set(String key, String value);

	public int getInt(String key);

	public double getDouble(String key);

	public boolean getBoolean(String key);

	public JSOModel getObject(String key);

	public JsArray<JSOModel> getArray(String key);
	
	public <T> ArrayList<T> getArrayList(String key, T type);

	public void set(String key, ArrayList<String> values);

	public void setArray(String key, ArrayList<JSOModel> values);

	public void set(String key, JSOModel object);

	public JsArrayString keys();
}
