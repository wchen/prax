package com.praxiteles.library.model;

import java.util.HashMap;

import com.praxiteles.library.AppConfiguration;
import com.praxiteles.library.field.Field;
import com.praxiteles.library.field.IntegerField;
import com.praxiteles.library.field.ModelField;
import com.praxiteles.library.field.StringField;

public class Icon extends ModelProxy {

	public final static IntegerField id;
	public final static StringField name;
	public final static StringField label;
	public final static StringField img;
	public final static IntegerField x;
	public final static IntegerField y;
	public final static IntegerField type;
	public final static ModelField<IconInfo> auxiliary;

	private static HashMap<String, Field> fieldsMap;

	static {
		id = new IntegerField("id");
		name = new StringField("name");
		label = new StringField("label");
		img = new StringField("img");
		x = new IntegerField("x");
		y = new IntegerField("y");
		type = new IntegerField("type");
		auxiliary = new ModelField<IconInfo>("auxiliary", new IconInfo());

		fieldsMap = new HashMap<String, Field>();
		put(fieldsMap, id);
		put(fieldsMap, name);
		put(fieldsMap, label);
		put(fieldsMap, img);
		put(fieldsMap, x);
		put(fieldsMap, y);
		put(fieldsMap, type);
		put(fieldsMap, auxiliary);
	}
	
	public Icon() {
		this((String) null);
	}

	public Icon(String id) {
		super(id);
	}

	public Icon(ModelInterface data) {
		super(data);
	}

	@Override
	public HashMap<String, Field> getFields() {
		return Icon.fieldsMap;
	}

	@Override
	public Field getIdentifierKey() {
		return Icon.id;
	}

	@Override
	public String getEndpoint() {
		return AppConfiguration.ICON_ENDPOINT;
	}

	@Override
	protected ModelVirtualProxy getModel(
			Class<? extends ModelVirtualProxy> modelType, JSOModel jsoModel) {
		return modelType == IconInfo.class ? new IconInfo(jsoModel) : null;
	}

	public String getPositionString() {
		return get(Icon.x) + "," + get(Icon.y);
	}
}
