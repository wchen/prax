package com.praxiteles.library.model;

import com.praxiteles.library.field.StringField;

public class Group extends ModelProxy {
	public final static StringField name = new StringField("name");

	public Group() {
		super();
	}

	public Group(ModelInterface data) {
		super(data);
	}
}