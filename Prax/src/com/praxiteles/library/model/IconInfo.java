package com.praxiteles.library.model;

import java.util.HashMap;

import com.praxiteles.library.field.ArrayListField;
import com.praxiteles.library.field.Field;
import com.praxiteles.library.field.ModelField;
import com.praxiteles.library.field.StringField;

public class IconInfo extends ModelProxy {

	public final static StringField url;
	public final static ModelField<Menu> rClickMenu;
	public final static ArrayListField<Icon> icons;

	private static HashMap<String, Field> fieldsMap;

	static {
		url = new StringField("url");
		rClickMenu = new ModelField<Menu>("rclick_menu", new Menu());
		icons = new ArrayListField<Icon>("icons", new Icon());

		fieldsMap = new HashMap<String, Field>();
		put(fieldsMap, url);
		put(fieldsMap, rClickMenu);
		put(fieldsMap, icons);
	}
	
	public IconInfo() {
		super();
	}

	public IconInfo(ModelInterface data) {
		super(data);
	}

	@Override
	public HashMap<String, Field> getFields() {
		return IconInfo.fieldsMap;
	}

	@Override
	protected ModelVirtualProxy getModel(
			Class<? extends ModelVirtualProxy> modelType, JSOModel jsoModel) {
		if (modelType == Menu.class) {
			return new Menu(jsoModel);
		} else if (modelType == Icon.class) {
			return new Icon(jsoModel);
		} else {
			return null;
		}
	}

}
