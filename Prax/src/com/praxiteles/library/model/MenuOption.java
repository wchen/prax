package com.praxiteles.library.model;

import java.util.HashMap;

import com.praxiteles.library.field.ArrayListField;
import com.praxiteles.library.field.Field;
import com.praxiteles.library.field.StringField;

public class MenuOption extends ModelProxy {

	public final static StringField name;
	public final static StringField event;
	public final static StringField img;
	public final static ArrayListField<MenuOption> menuOptions;

	private static HashMap<String, Field> fieldsMap;

	static {
		name = new StringField("name");
		event = new StringField("event");
		img = new StringField("img");
		menuOptions = new ArrayListField<MenuOption>("menu_options", new MenuOption());

		fieldsMap = new HashMap<String, Field>();
		put(fieldsMap, name);
		put(fieldsMap, event);
		put(fieldsMap, img);
		put(fieldsMap, menuOptions);
	}

	public MenuOption() {
		super();
	}

	public MenuOption(ModelInterface data) {
		super(data);
	}

	@Override
	public HashMap<String, Field> getFields() {
		return MenuOption.fieldsMap;
	}

	@Override
	protected ModelVirtualProxy getModel(
			Class<? extends ModelVirtualProxy> modelType, JSOModel jsoModel) {
		return modelType == MenuOption.class ? new MenuOption(jsoModel) : null;
	}

}
