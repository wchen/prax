package com.praxiteles.library.model;

import java.util.HashMap;

import com.praxiteles.library.AppConfiguration;
import com.praxiteles.library.field.ArrayListField;
import com.praxiteles.library.field.Field;
import com.praxiteles.library.field.IntegerField;

public class Dashboard extends ModelProxy {
	public final static ArrayListField<Workspace> workspaces;
	public final static IntegerField currentWorkspace;
	public final static IntegerField id;
	private static HashMap<String, Field> fieldsMap;

	static {
		workspaces = new ArrayListField<Workspace>("workspaces", new Workspace());
		currentWorkspace = new IntegerField("current_workspace");
		id = new IntegerField("id");
		fieldsMap = new HashMap<String, Field>();
		put(fieldsMap, id);
		put(fieldsMap, workspaces);
		put(fieldsMap, currentWorkspace);
	}

	public Dashboard() {
		this(null);
	}

	public Dashboard(String id) {
		super(id);
	}

	@Override
	public HashMap<String, Field> getFields() {
		return Dashboard.fieldsMap;
	}

	@Override
	public Field getIdentifierKey() {
		return Dashboard.id;
	}

	@Override
	public String getEndpoint() {
		return AppConfiguration.DASHBOARD_ENDPOINT;
	}

	@Override
	protected ModelVirtualProxy getModel(
			Class<? extends ModelVirtualProxy> modelType, JSOModel jsoModel) {
		return ((modelType == Workspace.class) ? new Workspace(jsoModel) : null);
	}
}
