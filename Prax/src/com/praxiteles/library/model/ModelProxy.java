package com.praxiteles.library.model;

import java.util.HashMap;

import com.praxiteles.library.field.Field;

/**
 * Model class is an abstract class that encapsulates all hideous remote server
 * requests, <br />
 * JSON to POJO conversions as well as field validations behind the scene. <br />
 * All model related server communications must go through this class.
 * 
 * @author wen
 * 
 */

public abstract class ModelProxy extends ModelRemoteProxy {

	private Field identifier;

	/* Constructors start */
	public ModelProxy() {
		this((ModelInterface) null);
	}

	/**
	 * Creates the ModelBase with identifier equal to id
	 * 
	 * @param id
	 *            String
	 */
	public ModelProxy(String id) {
		super((ModelInterface) null);
		if (id != null && id.length() > 0) {
			setIdentifier(id);
		}
	}

	/**
	 * Sets the end point of web service to the return value of getEndpoint()<br/>
	 * Sets identifier key to the return value of getIdentifierKey()<br/>
	 * Sets the data to jsoModel or create a new one if null is given.
	 * 
	 * @param jsoModel
	 *            JSOModel
	 */
	public ModelProxy(ModelInterface data) {
		super(data);
		setIdentifierKey(getIdentifierKey());
	}

	/* Constructors end */

	public Field getIdentifierKey() {
		return null;
	}

	public void setIdentifierKey(Field key) {
		if (key != null && key.toString().length() > 0)
			identifier = key;
	}

	public void setIdentifier(String value) {
		super.set(getIdentifierKey(), value);
	}

	public String getIdentifier() {
		return (String) get(identifier);
	}

	/**
	 * Save or update modified fields to web service<br/>
	 * Invokes save if identifier is null.
	 */
	public void save() {
		if (getEndpoint() == null) {
			return;
		}
		if (getIdentifier() == null) {
			super.post();
		} else {
			super.put();
		}
	}

	/**
	 * Set endpoint url with identifier and then send GET method to web service.
	 */
	@Override
	public void read() {
		if (getEndpoint() == null) {
			return;
		}
		service.setEndpoint(getEndpoint() + getIdentifier());
		super.read(null);
		service.setEndpoint(getEndpoint());
	}

	/**
	 * Set endpoint url with identifier and then send DELETE method to web
	 * service.
	 */
	@Override
	public void delete() {
		if (getEndpoint() == null) {
			return;
		}
		service.setEndpoint(getEndpoint() + getIdentifier());
		super.delete();
	}

	@Override
	protected void setRequestParamsFromModel() {
		super.setRequestParamsFromModel();
	}

	protected static void put(HashMap<String, Field> fieldArray, Field theField) {
		fieldArray.put(theField.toString(), theField);
	}
}