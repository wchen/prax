package com.praxiteles.library.model;

import java.util.HashMap;

import com.praxiteles.library.field.ArrayListField;
import com.praxiteles.library.field.Field;

public class Menu extends ModelProxy {

	public final static ArrayListField<MenuOption> menuOptions;

	private static HashMap<String, Field> fieldsMap;

	static {
		menuOptions = new ArrayListField<MenuOption>("menu_options", new MenuOption());

		fieldsMap = new HashMap<String, Field>();
		put(fieldsMap, menuOptions);
	}

	public Menu() {
		super();
	}

	public Menu(ModelInterface data) {
		super(data);
	}

	@Override
	public HashMap<String, Field> getFields() {
		return Menu.fieldsMap;
	}

	@Override
	protected ModelVirtualProxy getModel(
			Class<? extends ModelVirtualProxy> modelType, JSOModel jsoModel) {
		return modelType == MenuOption.class ? new MenuOption(jsoModel) : null;
	}
}
