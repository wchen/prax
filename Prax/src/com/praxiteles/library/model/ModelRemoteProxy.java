package com.praxiteles.library.model;

import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.praxiteles.library.Registry;
import com.praxiteles.library.WebServiceCommunicator;
import com.praxiteles.library.field.Field;
import com.praxiteles.library.service.ServiceError;

/**
 * Model class is an abstract class that encapsulates all hideous remote server
 * requests, <br />
 * JSON to POJO conversions as well as field validations behind the scene. <br />
 * All model related server communications must go through this class.
 * 
 * @author wen
 * 
 */
public abstract class ModelRemoteProxy extends ModelVirtualProxy {
	private abstract class ModelRequestCallback implements RequestCallback {

		/**
		 * On success:
		 * <ul>
		 * <li>Unmarshal Json response if read</li>
		 * <li>Set identifier field if save</li>
		 * <li>Or set all the fields sent by the server</li>
		 * </ul>
		 * On failure:
		 * <ul>
		 * <li>GWT log the error.</li>
		 * </ul>
		 */
		public void onResponseReceived(Request request, Response response) {
			String responseText = response.getText();
			if (response.getStatusCode() == 200) {
				GWT.log("Response return with status code "
						+ response.getStatusCode() + "\n" + responseText);
				if (responseText.length() > 0) {
					JSOModel responseData = JSOModel.fromJson(responseText);
					JsArrayString keys = responseData.keys();
					String key;
					Field field;
					bypassValidation();
					for (int i = 0, end = keys.length(); i < end; ++i) {
						key = keys.get(i);
						field = getFieldFromString(key);
						if (field != null)
							set(field, responseData.get(key));
						else
							GWT.log("Unexpected field: " + key + " found.");
					}
				}
			} else {
				GWT.log("Request failed with status code "
						+ response.getStatusCode() + ": "
						+ response.getStatusText() + "\nHeaders sent"
						+ response.getHeadersAsString() + "\n" + responseText);
				if (response.getStatusCode() == 400) {
					Registry.set(Registry.serviceErrorMsg, new ServiceError(
							responseText).get(ServiceError.error).toString());
				}
			}
		}

		public void onError(Request request, Throwable exception) {
			GWT.log("Request Error: " + exception.getMessage());
		}
	}

	/** Invokes custom callback if one is defined in ModelBase **/
	private class CustomUserRequestCallback extends ModelRequestCallback {

		@Override
		public void onResponseReceived(Request request, Response response) {
			super.onResponseReceived(request, response);
			if (getCustomCallback() instanceof RequestCallback) {
				customCallback.onResponseReceived(request, response);
			}
		}

		@Override
		public void onError(Request request, Throwable exception) {
			super.onError(request, exception);
			if (getCustomCallback() instanceof RequestCallback) {
				customCallback.onError(request, exception);
			}
		}
	}

	protected WebServiceCommunicator service;
	protected RequestCallback customCallback;

	/**
	 * Sets the end point of web service to the return value of getEndpoint()<br/>
	 * Sets identifier key to the return value of getIdentifierKey()<br/>
	 * Sets the data to jsoModel or create a new one if null is given.
	 * 
	 * @param jsoModel
	 *            JSOModel
	 */
	public ModelRemoteProxy() {
		super();
		service = getWebServiceCommunicator();
	}

	public ModelRemoteProxy(String jsoString) {
		super(jsoString);
		service = getWebServiceCommunicator();
	}

	public ModelRemoteProxy(ModelInterface data) {
		super();
		service = getWebServiceCommunicator();
		super.setData(data);
	}

	private WebServiceCommunicator getWebServiceCommunicator() {
		String endpoint = getEndpoint();
		if (endpoint != null && endpoint.length() > 0) {
			return new WebServiceCommunicator(endpoint);
		}
		return null;
	}

	public HashMap<String, Field> getFields() {
		return new HashMap<String, Field>();
	}

	public Field getFieldFromString(String key) {
		return (getFields() == null) ? null : getFields().get(key);
	}

	/* Web service start */
	public String getEndpoint() {
		return null;
	}

	// public void setEndpoint(String url) {
	// service.setEndpoint(url);
	// }

	public void setCustomCallback(RequestCallback callback) {
		customCallback = callback;
	}

	public RequestCallback getCustomCallback() {
		return customCallback;
	}

	public void setCallback(RequestCallback callback) {
		service.setCallback(callback);
	}

	/**
	 * Save or update modified fields to web service<br/>
	 * Invokes save if identifier is null.
	 */
	public void post() {
		if (getEndpoint() != null && super.isModified()) {
			super.setModified(false);
			service.setCallback(new CustomUserRequestCallback());
			setRequestParamsFromModel();
			service.post();
		}
	}

	public void put() {
		if (getEndpoint() != null && super.isModified()) {
			super.setModified(false);
			service.setCallback(new CustomUserRequestCallback());
			setRequestParamsFromModel();
			service.put();
		}
	}

	/**
	 * Set endpoint url with identifier and then send GET method to web service.
	 */
	public void read() {
		read(null);
	}

	public void read(HashMap<String, String> requestParams) {
		if (getEndpoint() == null)
			return;
		service.setCallback(new CustomUserRequestCallback());
		service.get(requestParams);
	}

	/**
	 * Set endpoint url with identifier and then send DELETE method to web
	 * service.
	 */
	public void delete() {
		if (getEndpoint() == null)
			return;
		service.setCallback(new CustomUserRequestCallback());
		service.delete();
	}

	// public void setHeader(String key, String value) {
	// service.setHeader(key, value);
	// }
	//
	// public String getHeader(String key) {
	// return service.getHeader(key);
	// }
	//
	// public String getRequestData() {
	// return service.getRequestData();
	// }
	//
	// public void setRequestData(String requestData) {
	// service.setRequestData(requestData);
	// }

	public void addRequestParameter(String key, String value) {
		// Make sure key and value are valid and not empty
		if (key != null && key.length() > 0 && value != null
				&& value.length() > 0)
			service.addRequestParameter(key, value);
		else
			GWT.log("invalid key value: " + key);
	}

	// public void removeRequestParameter(String key) {
	// service.removeRequestParameter(key);
	// }

	// public void addRequestParameter(HashMap<String, String> requestParams) {
	// service.addRequestParameter(requestParams);
	// }

	/**
	 * Looping through all data fields and add them as request parameters.
	 */
	protected void setRequestParamsFromModel() {
		Field field;
		String value;
		JsArrayString keys = super.keys();
		for (int i = 0; i < keys.length(); ++i) {
			field = getFieldFromString(keys.get(i));
			if (field != null && get(field) != null) {
				value = (String) get(field);
				addRequestParameter(keys.get(i), value);
			}
		}
	}

	public void resetRequestParameters() {
		service.resetRequestParameters();
	}

	// public boolean hasRequestParameter(String key) {
	// return service.hasRequestParameter(key);
	// }

	/* Web service end */
}