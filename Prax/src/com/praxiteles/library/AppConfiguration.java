package com.praxiteles.library;

/**
 * Application configurations are stored here.
 * 
 * @author Wensheng Chen
 *
 */
public class AppConfiguration {
	public static final String BASE_URL 			= "https://moondog.io";
	public static final String USER_ENDPOINT 		= BASE_URL + "/user/";
	public static final String AUTH_ENDPOINT 		= BASE_URL + "/auth/";
	public static final String ICON_ENDPOINT 		= BASE_URL + "/icon/";
	public static final String RECAPTCHA_ENDPOINT 	= BASE_URL + "/recaptcha/";
	public static final String DASHBOARD_ENDPOINT 	= BASE_URL + "/dashboard/";
	public static final String WORKSPACE_ENDPOINT 	= BASE_URL + "/workspace/";
	
	public static final String IMAGE_BASEURL 		= BASE_URL + "/images/";
	public static final String ICON_IMAGE_BASEURL 	= IMAGE_BASEURL + "/icons/";
	public static final int    ICON_X_WIDTH 		= 120;
	public static final int    ICON_Y_WIDTH 		= 120;
}
