package com.praxiteles.library.service;

import java.util.HashMap;

import com.google.gwt.http.client.RequestCallback;
import com.praxiteles.library.AppConfiguration;
import com.praxiteles.library.field.EmailField;
import com.praxiteles.library.field.Field;
import com.praxiteles.library.field.StringField;
import com.praxiteles.library.model.ModelRemoteProxy;

public class AuthenticateService extends ModelRemoteProxy {
	public final static StringField password;
	public final static EmailField email;
	private static HashMap<String, Field> fieldsMap;

	static {
		password = new StringField("password");
		email = new EmailField("email");

		fieldsMap = new HashMap<String, Field>();
		fieldsMap.put(password.toString(), password);
		fieldsMap.put(email.toString(), email);
	}

	private static AuthenticateService instance;

	private AuthenticateService() {
		super();
	}

	public static AuthenticateService instance() {
		if (instance == null) {
			instance = new AuthenticateService();
		}

		return instance;
	}

	public void authenticate(String email, String password,
			RequestCallback callback) {
		cleanup();
		set(AuthenticateService.email, email);
		set(AuthenticateService.password, password);
		super.setCustomCallback(callback);
		super.post();
	}

	private void cleanup() {
		super.setCallback(null);
		super.clearCachedFields();
		super.clearErrorMessages();
		super.resetRequestParameters();
	}

	@Override
	public HashMap<String, Field> getFields() {
		return AuthenticateService.fieldsMap;
	}

	@Override
	public String getEndpoint() {
		return AppConfiguration.AUTH_ENDPOINT;
	}

}
