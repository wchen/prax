package com.praxiteles.library.service;

import java.util.HashMap;

import com.google.gwt.http.client.RequestCallback;
import com.praxiteles.library.AppConfiguration;
import com.praxiteles.library.field.Field;
import com.praxiteles.library.field.StringField;
import com.praxiteles.library.model.ModelRemoteProxy;

public class RecaptchaService extends ModelRemoteProxy {
	public final static StringField challenge;
	public final static StringField response;
	private static HashMap<String, Field> fieldsMap;

	static {
		challenge = new StringField("challenge");
		response = new StringField("response");

		fieldsMap = new HashMap<String, Field>();
		fieldsMap.put(challenge.toString(), challenge);
		fieldsMap.put(response.toString(), response);
	}

	private static RecaptchaService instance;

	private RecaptchaService() {
		super();
	}

	public static RecaptchaService instance() {
		if (instance == null) {
			instance = new RecaptchaService();
		}

		return instance;
	}

	public void sendRecaptcha(String challenge, String response,
			RequestCallback callback) {
		cleanup();
		set(RecaptchaService.challenge, challenge);
		set(RecaptchaService.response, response);
		super.setCustomCallback(callback);
		super.post();
	}

	private void cleanup() {
		super.setCallback(null);
		super.clearCachedFields();
		super.clearErrorMessages();
		super.resetRequestParameters();
	}

	@Override
	public HashMap<String, Field> getFields() {
		return RecaptchaService.fieldsMap;
	}

	@Override
	public String getEndpoint() {
		return AppConfiguration.RECAPTCHA_ENDPOINT;
	}

}
