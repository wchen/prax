package com.praxiteles.library.service;

import java.util.HashMap;

import com.praxiteles.library.field.Field;
import com.praxiteles.library.field.StringField;
import com.praxiteles.library.model.JSOModel;
import com.praxiteles.library.model.ModelRemoteProxy;

public class ServiceError extends ModelRemoteProxy {
	public final static StringField error;
	private static HashMap<String, Field> fieldMap;

	static {
		error = new StringField("error");

		fieldMap = new HashMap<String, Field>();
		fieldMap.put(error.toString(), error);
	}

	@Override
	public HashMap<String, Field> getFields() {
		return ServiceError.fieldMap;
	}

	public ServiceError(String json) {
		super(JSOModel.fromJson(json));
	}

}
